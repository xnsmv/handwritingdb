# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

#    KATYA'S TABLES

class HwConcretization(models.Model):
    concretization_id = models.AutoField(primary_key=True)
    concretization = models.CharField(max_length=255)
    concretization_number = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.concretization

    class Meta:
        managed = True
        db_table = 'hw_concretization'
        verbose_name = 'Конкретизация'
        verbose_name_plural = '[hw] Конкретизации'


class HwElement(models.Model):
    element_id = models.AutoField(primary_key=True)
    element = models.CharField(max_length=255, blank=True, null=True)
    element_number = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.element

    class Meta:
        managed = True
        db_table = 'hw_element'
        verbose_name = 'Элемент'
        verbose_name_plural = '[hw] Элементы'


class HwGeneralSignType(models.Model):
    general_sign_type_id = models.AutoField(primary_key=True)
    general_sign_type = models.CharField(max_length=255, blank=True, null=True)
    general_sign_type_number = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.general_sign_type

    class Meta:
        managed = True
        db_table = 'hw_general_sign_type'
        verbose_name = 'Группа общих признаков'
        verbose_name_plural = '[hw] Группы общих признаков'


class HwGeneralSignValue(models.Model):
    general_sign_value_id = models.AutoField(primary_key=True)
    general_sign_type = models.ForeignKey(HwGeneralSignType, models.DO_NOTHING, blank=True, null=True)
    general_sign_value = models.CharField(max_length=255, blank=True, null=True)
    general_sign_value_number = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.general_sign_type}: {self.general_sign_value}'

    class Meta:
        managed = True
        db_table = 'hw_general_sign_value'
        verbose_name = 'Значение частного признака'
        verbose_name_plural = '[hw] Значения частных признаков'


class HwGeneralWriting(models.Model):
    general_writing_id = models.AutoField(primary_key=True)
    set = models.ForeignKey('HwSet', models.DO_NOTHING, blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    general_sign_value = models.ForeignKey(HwGeneralSignValue, models.DO_NOTHING, blank=True, null=True)
    amount_sign = models.IntegerField(blank=True, null=True)
    amount_all = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.set}, {self.general_sign_value}: встречается в {self.amount_sign} из {self.amount_all}'

    class Meta:
        managed = True
        db_table = 'hw_general_writing'
        verbose_name = 'Общий признаки'
        verbose_name_plural = '[hw] Общие признаки'


class HwImage(models.Model):
    image_id = models.AutoField(primary_key=True)
    set = models.ForeignKey('HwSet', models.DO_NOTHING, blank=True, null=True)
    image_path = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.set}: {self.image_path}'  # TODO: придумать вывод

    class Meta:
        managed = True
        db_table = 'hw_image'
        verbose_name = 'Изображение'
        verbose_name_plural = '[hw] Изображения'


class HwLetter(models.Model):
    letter_id = models.AutoField(primary_key=True)
    letter = models.CharField(max_length=255)
    letter_number = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.letter

    class Meta:
        managed = True
        db_table = 'hw_letter'
        verbose_name = 'Буква'
        verbose_name_plural = '[hw] Буквы'


class HwLetterConcretization(models.Model):
    letter_concretization_id = models.AutoField(primary_key=True)
    letter_element = models.ForeignKey('HwLetterElement', models.DO_NOTHING)
    concretization = models.ForeignKey(HwConcretization, models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.letter_element}: {self.concretization}'

    class Meta:
        managed = True
        db_table = 'hw_letter_concretization'
        verbose_name = 'Конкретизация буквенного элемента'
        verbose_name_plural = '[hw] Конкретизации буквенных элементов'


class HwLetterElement(models.Model):
    letter_element_id = models.AutoField(primary_key=True)
    letter = models.ForeignKey(HwLetter, models.DO_NOTHING)
    element = models.ForeignKey(HwElement, models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.element} {self.letter}'

    class Meta:
        managed = True
        db_table = 'hw_letter_element'
        verbose_name = 'Буквенный элемент'
        verbose_name_plural = '[hw] Буквенные элементы'


class HwLinkNext(models.Model):
    link_next_id = models.AutoField(primary_key=True)
    link_next = models.CharField(max_length=255)

    def __str__(self):
        return self.link_next

    class Meta:
        managed = True
        db_table = 'hw_link_next'
        verbose_name = 'Связь между буквами'
        verbose_name_plural = '[hw] Связи между буквами'


class HwPartialSign(models.Model):
    partial_sign_id = models.AutoField(primary_key=True)
    partial_sign_value = models.ForeignKey('HwPartialSignValue', models.DO_NOTHING, blank=True, null=True)
    partial_sign_value_concretization = models.ForeignKey('HwPartialSignValueConcretization', models.DO_NOTHING, blank=True, null=True)
    first_letter_concretization = models.ForeignKey(HwLetterConcretization, models.DO_NOTHING, blank=True, null=True, related_name='hw_first_partial_signs')
    second_letter_concretization = models.ForeignKey(HwLetterConcretization, models.DO_NOTHING, blank=True, null=True, related_name='hw_second_partial_writings')
    relation_between_concretizations = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.first_letter_concretization  # TODO: придумать вывод

    class Meta:
        managed = True
        db_table = 'hw_partial_sign'


class HwPartialSignType(models.Model):
    partial_sign_type_id = models.AutoField(primary_key=True)
    partial_sign_type = models.CharField(max_length=255)
    partial_sign_type_number = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.partial_sign_type

    class Meta:
        managed = True
        db_table = 'hw_partial_sign_type'
        verbose_name = 'Группа частных признаков'
        verbose_name_plural = '[hw] Группы частных признаков'


class HwPartialSignValue(models.Model):
    partial_sign_value_id = models.AutoField(primary_key=True)
    partial_sign_type = models.ForeignKey(HwGeneralSignType, models.DO_NOTHING, blank=True, null=True)
    partial_sign_value = models.CharField(max_length=255, blank=True, null=True)
    partial_sign_value_number = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.partial_sign_type}: {self.partial_sign_value}'

    class Meta:
        managed = True
        db_table = 'hw_partial_sign_value'
        verbose_name = 'Значение частных признаков'
        verbose_name_plural = '[hw] Значения частных признаков'


class HwPartialSignValueConcretization(models.Model):
    partial_sign_value_concretization_id = models.AutoField(primary_key=True)
    partial_sign_value_concretization = models.CharField(max_length=255, blank=True, null=True)
    partial_sign_value_concretization_number = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.partial_sign_value_concretization

    class Meta:
        managed = True
        db_table = 'hw_partial_sign_value_concretization'


class HwPartialWriting(models.Model):
    partial_writing_id = models.AutoField(primary_key=True)
    first_letter_transcription = models.ForeignKey('HwTranscription', models.DO_NOTHING, blank=True, null=True, related_name='hw_first_partial_writings')
    second_letter_transcription = models.ForeignKey('HwTranscription', models.DO_NOTHING, blank=True, null=True, related_name='hw_second_partial_writings')
    partial_sign = models.ForeignKey(HwPartialSign, models.DO_NOTHING, blank=True, null=True)
    amount_sign = models.IntegerField(blank=True, null=True)
    amount_all = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.first_letter_transcription #TODO: придумать вывод

    class Meta:
        managed = True
        db_table = 'hw_partial_writing'


class HwSet(models.Model):
    set_id = models.IntegerField(primary_key=True)
    person = models.ForeignKey('Person', models.DO_NOTHING, blank=True, null=True)
    type = models.ForeignKey('HwType', models.DO_NOTHING, blank=True, null=True)
    amount = models.IntegerField(blank=True, null=True)
    verity = models.IntegerField(blank=True, null=True)
    test_number = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.person}, {self.type}, количество: {self.amount}, достоверность: {self.verity}'

    class Meta:
        managed = True
        db_table = 'hw_set'
        verbose_name = 'Набор подписей/расшифровок'
        verbose_name_plural = '[hw] Наборы подписей/расшифровок'


class HwTranscription(models.Model):
    transcription_id = models.AutoField(primary_key=True)
    set = models.ForeignKey(HwSet, models.DO_NOTHING, blank=True, null=True)
    letter_place = models.IntegerField(blank=True, null=True)
    link_next = models.ForeignKey(HwLinkNext, models.DO_NOTHING, blank=True, null=True)
    letter = models.ForeignKey(HwLetter, models.DO_NOTHING, blank=True, null=True)
    variant = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.set}: {self.letter} в позиции {self.letter_place}'

    class Meta:
        managed = True
        db_table = 'hw_transcription'
        verbose_name = 'Транскрипция'
        verbose_name_plural = '[hw] Транскрипции'


class HwType(models.Model):
    type_id = models.AutoField(primary_key=True)
    type = models.CharField(max_length=255)

    def __str__(self):
        return self.type

    class Meta:
        managed = True
        db_table = 'hw_type'
        verbose_name = 'Тип набора'
        verbose_name_plural = '[hw] Типы набора'


class Person(models.Model):
    person_id = models.AutoField(primary_key=True)
    person_name = models.CharField(max_length=255, blank=True, null=True)
    person_surname = models.CharField(max_length=255, blank=True, null=True)
    person_patronymic = models.CharField(max_length=255, blank=True, null=True)
    birthday = models.DateTimeField(blank=True, null=True)
    position = models.ForeignKey('Position', models.DO_NOTHING, blank=True, null=True)
    sex = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return f'{self.person_surname} {self.person_name[0]}. {self.person_patronymic[0]}.'

    class Meta:
        managed = True
        db_table = 'person'
        ordering = ['person_id']
        verbose_name = 'Респондент'
        verbose_name_plural = '[gen] Респонденты'


class Position(models.Model):
    position_id = models.AutoField(primary_key=True)
    position = models.CharField(max_length=255)

    def __str__(self):
        return self.position

    class Meta:
        managed = True
        db_table = 'position'
        verbose_name = 'Должность'
        verbose_name_plural = '[gen] Должности'


class QAnswer(models.Model):
    answer_id = models.AutoField(primary_key=True)
    question_id = models.ForeignKey('QQuestion', models.DO_NOTHING, blank=True, null=True, db_column='question_id')
    number_in_question = models.IntegerField(blank=True, null=True)
    answer_text_id = models.ForeignKey('QAnswerText', models.DO_NOTHING, blank=True, null=True, db_column='answer_text_id')
    score = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.number_in_question}. {self.answer_text}'

    class Meta:
        managed = True
        db_table = 'q_answer'
        verbose_name = 'Связь ответ-вопрос'
        verbose_name_plural = '[qnr] Связи ответ-вопрос'


class QAnswerText(models.Model):
    answer_text_id = models.AutoField(primary_key=True)
    answer_text = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.answer_text

    class Meta:
        managed = True
        db_table = 'q_answer_text'
        verbose_name = 'Текст ответа'
        verbose_name_plural = '[qnr] Тексты ответов'


class QComment(models.Model):
    comment_id = models.AutoField(primary_key=True)
    comment_text = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.comment_text

    class Meta:
        managed = True
        db_table = 'q_comment'
        verbose_name = 'Комментарий к разбиению'
        verbose_name_plural = '[qnr] Комментарии к разбиениям'


class QIntervalName(models.Model):
    interval_name_id = models.AutoField(primary_key=True)
    interval_nickname = models.CharField(max_length=255, blank=True, null=True)
    interval_name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.interval_nickname} ({self.interval_name})'

    class Meta:
        managed = True
        db_table = 'q_interval_name'
        verbose_name = 'Название интервала'
        verbose_name_plural = '[qnr] Названия интервалов'


class QIntervalScore(models.Model):
    interval_score_id = models.AutoField(primary_key=True)
    min_score = models.IntegerField(blank=True, null=True)
    max_score = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.min_score} - {self.max_score}'

    class Meta:
        managed = True
        db_table = 'q_interval_score'
        verbose_name = 'Числовой интервал'
        verbose_name_plural = '[qnr] Числовые интервалы'


class QIntervalSet(models.Model):
    interval_set_id = models.AutoField(primary_key=True)
    interval_set_name_id = models.ForeignKey('main.QIntervalSetName', models.DO_NOTHING, blank=True, null=True, db_column='interval_set_name_id')
    interval_name_id = models.ForeignKey('main.QIntervalName', models.DO_NOTHING, blank=True, null=True, db_column='interval_name_id')
    number_in_interval_set = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.interval_set_name_id} | {self.number_in_interval_set}. {self.interval_name_id}'

    class Meta:
        managed = True
        db_table = 'q_interval_set'
        verbose_name = 'Интервал шкалы'
        verbose_name_plural = '[qnr] Интервалы шкалы'


class QIntervalSetName(models.Model):
    interval_set_name_id = models.AutoField(primary_key=True)
    interval_set_name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.interval_set_name

    class Meta:
        managed = True
        db_table = 'q_interval_set_name'
        verbose_name = 'Название набора интервалов'
        verbose_name_plural = '[qnr] Названия наборов интервалов'


class QPartitioning(models.Model):
    partitioning_id = models.AutoField(primary_key=True)
    partitioning_scale_id = models.ForeignKey('main.QPartitioningScale', models.DO_NOTHING, blank=True, null=True,
                                              db_column='partitioning_scale_id')
    number_in_partitioning = models.IntegerField(blank=True, null=True)

    def __str__(self):  # TODO: Придумать вывод
        return self.number_in_partitioning

    class Meta:
        managed = True
        db_table = 'q_partitioning'
        verbose_name = 'Связь интервал-разбиение'
        verbose_name_plural = '[qnr] Связи интервал-разбиение'


class QPartitioningScale(models.Model):
    partitioning_scale_id = models.AutoField(primary_key=True)
    scale_id = models.ForeignKey('QScale', models.DO_NOTHING, blank=True, null=True, db_column='scale_id')
    partitioning_scale_name = models.CharField(max_length=255, blank=True, null=True)
    interval_set_id = models.ForeignKey('QIntervalSet', models.DO_NOTHING, blank=True, null=True,
                                        db_column='interval_set_id')

    def __str__(self):
        return self.partitioning_scale_name

    class Meta:
        managed = True
        db_table = 'q_partitioning_scale'
        verbose_name = 'Связь разбиение-шкала'
        verbose_name_plural = '[qnr] Связи разбиение-шкала'


class QPersonSelection(models.Model):
    person_selection_id = models.AutoField(primary_key=True)
    person_id = models.ForeignKey('main.Person', null=True, blank=True, on_delete=models.SET_NULL, db_column='person_id')
    answer_id = models.ForeignKey('main.QAnswer', null=True, blank=True, on_delete=models.SET_NULL, db_column='answer_id')

    def __str__(self):
        return f'{self.person_id}: {self.answer_id}'

    class Meta:
        managed = True
        db_table = 'q_person_selection'
        verbose_name = 'Выбор респондента'
        verbose_name_plural = '[qnr] Выборы респондентов'


class QQuestion(models.Model):
    question_id = models.AutoField(primary_key=True)
    number_in_questionnaire = models.IntegerField(blank=True, null=True)
    questionnaire = models.ForeignKey('QQuestionnaire', models.DO_NOTHING, blank=True, null=True)
    question_text = models.ForeignKey('QQuestionText', models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.questionnaire} | {self.number_in_questionnaire}. {self.question_text}'

    class Meta:
        managed = True
        db_table = 'q_question'
        ordering = ['question_id']
        verbose_name = 'Связь вопрос-опросник'
        verbose_name_plural = '[qnr] Связи вопрос-опросник'


class QQuestionScale(models.Model):
    question_scale_id = models.AutoField(primary_key=True)
    question_id = models.ForeignKey('QQuestion', models.DO_NOTHING, blank=True, null=True, db_column='question_id')
    scale_id = models.ForeignKey('QScale', models.DO_NOTHING, blank=True, null=True, db_column='scale_id')

    def __str__(self):
        return self.scale_id

    class Meta:
        managed = True
        db_table = 'q_question_scale'
        verbose_name = 'Связь вопрос-шкала'
        verbose_name_plural = '[qnr] Связи вопрос-шкала'


class QQuestionText(models.Model):
    question_text_id = models.AutoField(primary_key=True)
    question_text = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.question_text

    class Meta:
        managed = True
        db_table = 'q_question_text'
        ordering = ['question_text_id']
        verbose_name = 'Текст вопроса'
        verbose_name_plural = '[qnr] Тексты вопросов'


class QQuestionnaire(models.Model):
    questionnaire_id = models.AutoField(primary_key=True)
    questionnaire_nickname = models.CharField(max_length=255, blank=True, null=True)
    questionnaire_name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.questionnaire_nickname

    class Meta:
        managed = True
        db_table = 'q_questionnaire'
        ordering = ['questionnaire_id']
        verbose_name = 'Опросник'
        verbose_name_plural = '[qnr] Опросники'


class QResult(models.Model):
    result_id = models.AutoField(primary_key=True)
    person_id = models.ForeignKey('Person', models.DO_NOTHING, blank=True, null=True, db_column='person_id')
    scale_id = models.ForeignKey('QScale', models.DO_NOTHING, blank=True, null=True, db_column='scale_id')
    raw_score = models.IntegerField(blank=True, null=True)
    result_score = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.person_id}: {self.result_score} баллов'

    class Meta:
        managed = True
        db_table = 'q_result'
        verbose_name = 'Результат тестов'
        verbose_name_plural = '[qnr] Результаты тестов'


class QScale(models.Model):
    scale_id = models.AutoField(primary_key=True)
    scale_name = models.CharField(max_length=255, blank=True, null=True)
    scale_nickname = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.scale_name

    class Meta:
        managed = True
        db_table = 'q_scale'
        verbose_name = 'Шкала опросников'
        verbose_name_plural = '[qnr] Шкалы опросников'
