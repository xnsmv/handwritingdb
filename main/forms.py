from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from django import forms
from .models import *

from django.forms.widgets import ClearableFileInput


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']


SEX_CHOICES = [
    ('', '---------'),
    ('м', 'муж.'),
    ('ж', 'жен.'),
]


class PersonForm(forms.ModelForm):
    person_surname = forms.CharField(required=True, label="Фамилия")
    person_name = forms.CharField(required=True, label="Имя")
    birthday = forms.DateField(required=False, label="Дата рождения")
    # sex = forms.ChoiceField(choices=SEX_CHOICES, label="Пол")
    sex = forms.CharField(required=False, widget=forms.Select(choices=SEX_CHOICES), label="Пол")

    class Meta:
        model = Person
        fields = ('person_surname',
                  'person_name',
                  'person_patronymic',
                  'birthday',
                  'position',
                  'sex')
        labels = {
            "person_surname": "Фамилия",
            "person_name": "Имя",
            "person_patronymic": "Отчество",
            "birthday": "Дата рождения",
            "position": "Должность",
            "sex": "Пол",
        }


class SetForm(forms.ModelForm):
    verity = forms.BooleanField(required=False, label='Достоверно ли множество')

    class Meta:
        model = HwSet
        fields = ('type',
                  'amount',
                  # 'verity',
                  'test_number',
                  'person')
        labels = {
            'type': 'Тип',
            'amount': 'Количество',
            # 'verity': 'Достоверность',
            'test_number': 'Номер тестового образца',
        }

    def __init__(self, *args, **kwargs):
        person = kwargs.pop('person')
        verity = kwargs.pop('verity')
        super(SetForm, self).__init__(*args, **kwargs)
        self.fields['person'].initial = person
        self.fields['person'].widget = forms.HiddenInput()

        if verity == 0:
            self.fields['verity'].initial = None
        else:
            self.fields['verity'].initial = verity

    def clean_verity(self):
        return 1 if self.cleaned_data['verity'] and self.cleaned_data['verity'] != 0 else 0

    def clean_testnumber(self):
        return self.cleaned_data['test_number'] if self.cleaned_data['test_number'] else '0'


class GenForm(forms.ModelForm):
    set = forms.ModelChoiceField(queryset=None, label='Множество')
    general_sign_value = forms.ChoiceField(label='Значение')
    amount = forms.IntegerField(label='Проявление')
    person = forms.IntegerField()

    class Meta:
        model = HwGeneralSignValue
        fields = ('set', 'general_sign_type', 'general_sign_value')
        labels = {
            "general_sign_type": "Признак"
        }

    def __init__(self, *args, **kwargs):
        person = kwargs.pop('person')
        super(GenForm, self).__init__(*args, **kwargs)
        self.fields['person'].initial = person
        self.fields['set'].queryset = HwSet.objects.filter(person_id__exact=person)
        self.fields['general_sign_value'].queryset = HwGeneralSignValue.objects.none()
        self.fields['person'].widget = forms.HiddenInput()

        if 'general_sign_type' in self.data:
            try:
                general_sign_type_id = int(self.data.get('general_sign_type'))
                self.fields['general_sign_value'].queryset = HwGeneralSignValue.objects.filter(
                    general_sign_type_id=general_sign_type_id)
            except (ValueError, TypeError):
                pass

        elif self.instance.pk:
            print(self.instance.pk)
            self.fields['general_sign_value'].queryset = self.instance.general_sign_type


class PartForm(forms.Form):
    set = forms.ModelChoiceField(required=True, queryset=None, label='Выберите множество')
    first_letter = forms.ModelChoiceField(required=False, queryset=None, label='Первая буква')
    first_letter_element = forms.ModelChoiceField(required=False, queryset=None, label='Элемент первой буквы')
    first_letter_element_concretization = forms.ModelChoiceField(required=True, queryset=None,
                                                                 label='Конкретизация элемента первой буквы')

    second_letter = forms.ModelChoiceField(queryset=None, required=False, label='Вторая буква')
    second_letter_element = forms.ModelChoiceField(queryset=HwLetterElement.objects.all(), required=False,
                                                   label='Элемент второй буквы')
    second_letter_element_concretization = forms.ModelChoiceField(queryset=HwLetterConcretization.objects.all(),
                                                                  required=False,
                                                                  label='Конкретизация элемента второй буквы')

    partial_sign_type = forms.ModelChoiceField(required=True, queryset=HwPartialSignType.objects.all(), label='Признак')
    partial_sign_value = forms.ModelChoiceField(required=True, queryset=HwPartialSignValue.objects.all(),
                                                label='Значение')
    partial_sign_value_concretization = forms.ModelChoiceField(required=False,
                                                               queryset=HwPartialSignValueConcretization.objects.all(),
                                                               label='Конкретизация значения')

    relation_between_concretizations = forms.BooleanField(required=False, label='Связь между конкретизациями букв')
    amount = forms.IntegerField(required=False, label='Проявление')
    person = forms.IntegerField()

    class Meta:
        model = HwPartialSign
        fields = ('set',
                  # 'first_letter',
                  'first_letter_element',
                  'first_letter_element_concretization',
                  'second_letter',
                  'second_letter_element',
                  'second_letter_element_concretization',
                  'partial_sign_type',
                  'partial_sign_value',
                  'relation_between_concretizations',
                  'partial_sign_value_concretization',
                  'amount')
        labels = {
            "partial_sign_type": "Признак"
        }

    def __init__(self, *args, **kwargs):
        person = kwargs.pop('person')
        super(PartForm, self).__init__(*args, **kwargs)
        self.fields['person'].initial = person
        self.fields['person'].widget = forms.HiddenInput()
        self.fields['set'].queryset = HwSet.objects.filter(person_id__exact=person)
        self.fields['first_letter'].queryset = HwTranscription.objects.all()
        self.fields['first_letter_element'].queryset = HwLetterElement.objects.all()
        self.fields['first_letter_element_concretization'].queryset = HwLetterConcretization.objects.all()
        self.fields['second_letter'].queryset = HwTranscription.objects.all()

    def clean_relation(self):
        return 1 if self.cleaned_data['relation_between_concretizations'] and self.cleaned_data[
            'relation_between_concretizations'] != 0 else 0


class TransForm(forms.ModelForm):
    # set = forms.ChoiceField()
    person = forms.IntegerField()
    variant = forms.BooleanField(required=False, label='Вариант')

    # trans = forms.CharField(widget=forms.Textarea)
    # transcription = forms.CharField()

    class Meta:
        model = HwTranscription
        fields = ('set',
                  # 'transcription',
                  'letter',
                  'letter_place',
                  'link_next',
                  # 'variant'
                  )
        labels = {
            "set": "Множество",
            "letter": "Буква",
            "letter_place": "Место",
            "link_next": "Связь",
        }

    def __init__(self, *args, **kwargs):
        person = kwargs.pop('person')
        super(TransForm, self).__init__(*args, **kwargs)
        self.fields['person'].initial = person
        self.fields['person'].widget = forms.HiddenInput()
        self.fields['set'].queryset = HwSet.objects.filter(person_id__exact=person)


class ImgForm(forms.ModelForm):
    # image = forms.ImageField(HwImage, widget=forms.FileInput())

    class Meta:
        model = HwImage
        fields = ('image', 'set')
        labels = {"image": "", }

    def __init__(self, *args, **kwargs):
        set = kwargs.pop('set')
        super(ImgForm, self).__init__(*args, **kwargs)
        self.fields['set'].initial = set
        self.fields['set'].widget = forms.HiddenInput()
        self.fields['image'].widget = MyClearableFileInput()


class MyClearableFileInput(ClearableFileInput):
    initial_text = 'Активное изображение'
