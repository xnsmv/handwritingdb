from main.domain.service_functions import divide_by_sex, \
    count_persons_w_tests_n_hw, prepare_examples_q, prepare_examples_hw, \
    divide_by_result
from main.domain.jsm_func import sort_hypotheses_2, jsm_create_similarities, jsm_filtration, sort_hypotheses
from main.domain.model_getters import get_scales_by_questionnaire, \
    get_results_by_questionnaire, get_assessments_by_questionnaire
from main.models import HwSet, QResult


# Нахождение потенциальных гипотез
def create_potential_hypotheses(questionnaire_id=1, partitioning_scale_name_id=2, resps_minimum=2, split_func=divide_by_sex, sim_signs='gen_elem', **div_params):

    resps_list = count_persons_w_tests_n_hw(questionnaire_id)

    # QUESTIONNAIRES

    sorted_similarities_plus_q, sorted_similarities_minus_q = similarities_q_func(
        questionnaire_id,
        partitioning_scale_name_id,
        resps_minimum,
        split_func,
        resps_list,
        q_or_hw='q', **div_params)

    sorted_q_plus, biggest_hypothesis_q_plus = sort_hypotheses_2(sorted_similarities_plus_q)
    sorted_q_minus, biggest_hypothesis_q_minus = sort_hypotheses_2(sorted_similarities_minus_q)

    # HANDWRITINGS

    sorted_similarities_plus_hw, sorted_similarities_minus_hw = similarities_hw_func(
        resps_list,
        resps_minimum,
        sim_signs,
        split_func,
        q_or_hw='hw', **div_params)

    sorted_hw_plus, biggest_hypothesis_hw_plus = sort_hypotheses_2(
        sorted_similarities_plus_hw)
    sorted_hw_minus, biggest_hypothesis_hw_minus = sort_hypotheses_2(
        sorted_similarities_minus_hw)

    # добавляем по пустому множеству к каждому виду гипотез

    sorted_hypotheses_q_plus = []
    sorted_hypotheses_q_minus = []

    for h in sorted_q_plus:
        sorted_hypotheses_q_plus.append([h[1], h[0], set()])
    for h in sorted_q_minus:
        sorted_hypotheses_q_minus.append([h[1], h[0], set()])

    sorted_hypotheses_hw_plus = []
    sorted_hypotheses_hw_minus = []

    for h in sorted_hw_plus:
        sorted_hypotheses_hw_plus.append([h[1], set(), h[0]])
    for h in sorted_hw_minus:
        sorted_hypotheses_hw_minus.append([h[1], set(), h[0]])

    # 3. Пересекаем
    # объединяем гипотезы
    all_hypotheses_plus = sorted_hypotheses_hw_plus + sorted_hypotheses_q_plus
    sim_all_plus = jsm_create_similarities(all_hypotheses_plus, flag=False)
    filtered_sim_all_plus = jsm_filtration(sim_all_plus, int_minimum=resps_minimum, extended=True)
    sorted_sim_all_plus = sort_hypotheses(filtered_sim_all_plus, person_index=0)

    all_hypotheses_minus = sorted_hypotheses_hw_minus + sorted_hypotheses_q_minus
    sim_all_minus = jsm_create_similarities(all_hypotheses_minus, flag=False)
    filtered_sim_all_minus = jsm_filtration(sim_all_minus, int_minimum=resps_minimum, extended=True)
    sorted_sim_all_minus = sort_hypotheses(filtered_sim_all_minus, person_index=0)
    return sorted_sim_all_plus, sorted_sim_all_minus, sorted_hypotheses_hw_plus, \
        sorted_hypotheses_hw_minus, sorted_hypotheses_q_plus, sorted_hypotheses_q_minus


# классифицировать гипотезы по прямому/обратному методу
def classify_potential_hypotheses(sorted_sim_all_plus, sorted_sim_all_minus,
                                  sorted_hypotheses_hw_plus, sorted_hypotheses_hw_minus,
                                  sorted_hypotheses_q_plus, sorted_hypotheses_q_minus):

    q_resp = []
    hw_resp = []
    for i in sorted_hypotheses_q_plus:
        q_resp.append(i[0])
    for i in sorted_hypotheses_hw_plus:
        hw_resp.append(i[0])
    direct_jsm_plus = []
    inverse_jsm_plus = []
    both_jsm_plus = []
    neither_jsm_plus = []
    for hypo in sorted_sim_all_plus:
        if hypo[0] in q_resp and hypo[0] in hw_resp:
            both_jsm_plus.append(hypo)
        elif hypo[0] in hw_resp:
            # print('обратный метод: ', hypo)
            inverse_jsm_plus.append(hypo)
        elif hypo[0] in q_resp:
            # print('прямой метод: ', hypo)
            direct_jsm_plus.append(hypo)
            # print('и прямой, и обратный: ', hypo)
        else:
            neither_jsm_plus.append(hypo)
            # print('не является гипотезой: ', hypo)

    for i in sorted_hypotheses_q_minus:
        q_resp.append(i[0])
    for i in sorted_hypotheses_hw_minus:
        hw_resp.append(i[0])
    direct_jsm_minus = []
    inverse_jsm_minus = []
    both_jsm_minus = []
    neither_jsm_minus = []
    for hypo in sorted_sim_all_minus:
        if hypo[0] in q_resp and hypo[0] in hw_resp:
            both_jsm_minus.append(hypo)
        elif hypo[0] in hw_resp:
            # print('обратный метод: ', hypo)
            inverse_jsm_minus.append(hypo)
        elif hypo[0] in q_resp:
            # print('прямой метод: ', hypo)
            direct_jsm_minus.append(hypo)
            # print('и прямой, и обратный: ', hypo)
        else:
            neither_jsm_minus.append(hypo)
            # print('не является гипотезой: ', hypo)

    return direct_jsm_plus, inverse_jsm_plus, both_jsm_plus, neither_jsm_plus, \
           direct_jsm_minus, inverse_jsm_minus, both_jsm_minus, neither_jsm_minus


def similarities_q_func(questionnaire_id, partitioning_scale_name_id, resps_minimum, split_func, resps_list=None, q_or_hw='q', **div_params):

    scales = get_scales_by_questionnaire(questionnaire_id)
    if resps_list is None:
        results = get_results_by_questionnaire(questionnaire_id)
    else:
        results = get_results_by_questionnaire(questionnaire_id).filter(person_id__in=resps_list)

    assessments = get_assessments_by_questionnaire(questionnaire_id,
                                                   partitioning_scale_name_id,
                                                   scales)

    sorted_similarities_plus, sorted_similarities_minus = \
        create_similarities(q_or_hw, resps_minimum,
                            results=results,
                            questionnaire_id=questionnaire_id,
                            partitioning_scale_name_id=partitioning_scale_name_id,
                            assessments=assessments,
                            split_func=split_func, **div_params)

    return sorted_similarities_plus, sorted_similarities_minus


def similarities_hw_func(resps_list, resps_minimum, sim_signs, split_func, q_or_hw='hw', **div_params):
    ordered_sets = HwSet.objects.filter(verity=1, person_id__in=resps_list) \
        .order_by('person_id')

    sorted_similarities_plus, sorted_similarities_minus = \
        create_similarities(q_or_hw, resps_minimum,
                            sim_signs=sim_signs,
                            ordered_sets=ordered_sets,
                            split_func=split_func, **div_params)

    return sorted_similarities_plus, sorted_similarities_minus


def create_similarities(q_or_hw, resps_minimum, **kwargs):
    examples_plus, examples_minus = None, None
    if q_or_hw == 'q':
        examples_plus, examples_minus = prepare_examples_q(**kwargs)
    elif q_or_hw == 'hw':
        examples_plus, examples_minus = prepare_examples_hw(**kwargs)

    # находим сходства
    similarities_plus = jsm_create_similarities(examples_plus)
    similarities_minus = jsm_create_similarities(examples_minus)

    # Фильтруем чтобы было минимум resps_minimum респондентов в сходстве
    filtered_similarities_plus = jsm_filtration(similarities_plus,
                                                ext_minimum=resps_minimum)
    filtered_similarities_minus = jsm_filtration(similarities_minus,
                                                 ext_minimum=resps_minimum)

    # сортируем так чтобы сходства располагались по подможествам респондентов
    sorted_similarities_plus = sort_hypotheses(filtered_similarities_plus)
    sorted_similarities_minus = sort_hypotheses(filtered_similarities_minus)

    return sorted_similarities_plus, sorted_similarities_minus


# получить параметры разделения примеров на + и -
def get_div_params(div_option, scale_id=11, div_value=3):

    split_func = None
    div_plus = ''
    div_minus = ''
    div_params = dict()

    if div_option == 1:
        split_func = divide_by_sex
        div_plus = 'женский пол'
        div_minus = 'мужской пол'
    elif div_option == 2:
        res_plus = QResult.objects.filter(scale_id=scale_id,
                                                result_score__lte=div_value).values_list(
            'person_id', flat=True)
        res_minus = QResult.objects.filter(scale_id=scale_id,
                                                 result_score__gt=div_option).values_list(
            'person_id', flat=True)
        div_params['res_plus'] = res_plus
        div_params['res_minus'] = res_minus

        split_func = divide_by_result
        div_plus = 'возбудимость низкая (<4 стенов)'
        div_minus = 'возбудимость средняя (>3 стенов)'

    return split_func, div_plus, div_minus, div_params