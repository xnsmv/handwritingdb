# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.template.defaultfilters import upper


# Примеры: точка начала; точка соединения 2 и 3 элемента
class HwConcretization(models.Model):
    concretization_id = models.AutoField(primary_key=True, editable=False)
    concretization = models.CharField(max_length=255)
    concretization_number = models.PositiveIntegerField(blank=True, null=True, editable=False)

    def __str__(self):
        return self.concretization

    def get_number(self):
        return self.concretization_number

    class Meta:
        managed = True
        db_table = 'hw_concretization'
        verbose_name = 'Конкретизация'
        verbose_name_plural = 'Конкретизации'


# Пример: 1 элемент; петлевая часть
class HwElement(models.Model):
    element_id = models.AutoField(primary_key=True, editable=False)
    element = models.CharField(max_length=255, blank=True, null=True)
    element_number = models.PositiveIntegerField(blank=True, null=True, editable=False)

    def __str__(self):
        return self.element

    class Meta:
        managed = True
        db_table = 'hw_element'
        verbose_name = 'Элемент'
        verbose_name_plural = 'Элементы'


# Пример: нажим; темп движений
class HwGeneralSignType(models.Model):
    general_sign_type_id = models.AutoField(primary_key=True, editable=False)
    general_sign_type = models.CharField(max_length=255, blank=True, null=True)
    general_sign_type_number = models.PositiveIntegerField(blank=True, null=True, editable=False)

    def __str__(self):
        return self.general_sign_type

    class Meta:
        managed = True
        db_table = 'hw_general_sign_type'
        verbose_name = 'Группа общих признаков'
        verbose_name_plural = 'Группы общих признаков'
        ordering = ['general_sign_type_number']


UNINFORMATIVE_GSVALUES = [
    '1;1',
    '5;1',
    '6;1',
    '6;3',
    '7;2',
    '8;3',
    '9;1',
    '10;3',
    '11;1',
    '12;1',
]


# Пример: темп движений: низкий
class HwGeneralSignValue(models.Model):
    general_sign_value_id = models.AutoField(primary_key=True, editable=False)
    general_sign_type = models.ForeignKey('HwGeneralSignType', models.DO_NOTHING, blank=True, null=True)
    general_sign_value = models.CharField(max_length=255, blank=True, null=True)
    general_sign_value_number = models.PositiveIntegerField(blank=True, null=True, editable=False)
    jsm_number = models.CharField(max_length=255, blank=True, null=True)

    def get_sign_type_object(self):
        return self.general_sign_type

    def get_type_number(self):
        return f'{self.general_sign_type.general_sign_type_number};'

    def get_sign_type_str(self):
        return f'{self.general_sign_type}'

    def get_general_value(self):
        return f'(общ.)({self.get_number()}) {self.general_sign_type}: {self.general_sign_value}'

    def get_jsm_number(self):
        return f'({self.jsm_number})'

    def __str__(self):
        return f'{self.general_sign_type}: {self.general_sign_value}'

    def jsm_number_print(self):
        s = f'({self.jsm_number})'
        if self.jsm_number in UNINFORMATIVE_GSVALUES:
            s += '(неинформ.)'
        #s += f' {self.general_sign_type}: {self.general_sign_value}'
        return s

    def jsm_number_print_potential(self):
        return self.jsm_number_print()

    def jsm_value_print(self):
        return f' {self.general_sign_type}: {self.general_sign_value}'

    def jsm_value_print_potential(self):
        return self.jsm_value_print()

    def get_number(self):
        return f'{self.general_sign_type.general_sign_type_number};{self.general_sign_value_number}'

    def get_dropdown_value(self):
        return self.general_sign_value

    def get_str_type(self):
        return self.general_sign_type

    def get_str_value(self):
        return self.general_sign_value

    class Meta:
        managed = True
        db_table = 'hw_general_sign_value'
        verbose_name = 'Значение общего признака'
        verbose_name_plural = 'Значения общих признаков'
        ordering = ['general_sign_type__general_sign_type_number', 'general_sign_value_number']


# Пример: Охлупина А. Н., тестовый образец № 2, наклон подписи: правый: встречается в 1 из 1
class HwGeneralWriting(models.Model):
    general_writing_id = models.AutoField(primary_key=True, editable=False)
    set = models.ForeignKey('HwSet', models.DO_NOTHING, blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    general_sign_value = models.ForeignKey('HwGeneralSignValue', models.DO_NOTHING, blank=True, null=True)
    amount_sign = models.PositiveIntegerField(blank=True, null=True)
    amount_all = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.set}, {self.general_sign_value}: встречается в {self.amount_sign} из {self.amount_all}'

    def get_absolute_url(self):
        return 'gen_writing/%s/'%(self.general_writing_id)

    class Meta:
        managed = True
        db_table = 'hw_general_writing'
        verbose_name = 'Общий признаки'
        verbose_name_plural = 'Общие признаки'


# путь к фотографии подписи
class HwImage(models.Model):
    image_id = models.AutoField(primary_key=True)
    set = models.ForeignKey('HwSet', models.DO_NOTHING, blank=True, null=True)
    image_path = models.CharField(max_length=255, blank=True, null=True)
    image = models.ImageField(upload_to='set_image', blank=True)

    def __str__(self):
        return f'{self.set}: {self.image_path}'  # TODO: придумать вывод

    class Meta:
        managed = True
        db_table = 'hw_image'
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'


# Пример: Й; дугообразный элемент
class HwLetter(models.Model):
    letter_id = models.AutoField(primary_key=True, editable=False)
    letter = models.CharField(max_length=255)
    letter_number = models.PositiveIntegerField(blank=True, null=True, editable=False)

    def __str__(self):
        return self.letter

    def get_dropdown_value(self):
        return self.letter

    class Meta:
        managed = True
        db_table = 'hw_letter'
        verbose_name = 'Буква'
        verbose_name_plural = 'Буквы'


# Пример: точка начала 1 элемент Р
class HwLetterConcretization(models.Model):
    letter_concretization_id = models.AutoField(primary_key=True, editable=False)
    letter_element = models.ForeignKey('HwLetterElement', models.DO_NOTHING, blank=True, null=True)
    concretization = models.ForeignKey('HwConcretization', models.DO_NOTHING, blank=True, null=True)

    def get_number(self):
        # j:k1, k2
        s = f'{self.letter_element.letter.letter_number}:{self.letter_element.element.element_number},'
        if self.concretization:
            s += f'{self.concretization.concretization_number}'
        else:
            s += '0'
        return s

    def __str__(self):
        return f'{self.concretization} {self.letter_element} '

    def get_dropdown_value(self):
        return self.concretization

    class Meta:
        managed = True
        db_table = 'hw_letter_concretization'
        verbose_name = 'Конкретизация буквенного элемента'
        verbose_name_plural = 'Конкретизации буквенных элементов'


# Пример: вся буква угловатый элемент
class HwLetterElement(models.Model):
    letter_element_id = models.AutoField(primary_key=True, editable=False)
    letter = models.ForeignKey('HwLetter', models.DO_NOTHING, blank=True, null=True)
    element = models.ForeignKey('HwElement', models.DO_NOTHING, blank=True, null=True)
    general_element = models.ForeignKey('HwGeneralElement', models.DO_NOTHING, blank=True, null=True, related_name='hw_gen_el')
    optional_general_element = models.ForeignKey('HwGeneralElement', models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.element} {self.letter}'

    def get_dropdown_value(self):
        return self.element

    def get_number(self):
        return f'{self.letter.letter_number}:{self.element.element_number}'

    class Meta:
        managed = True
        db_table = 'hw_letter_element'
        verbose_name = 'Буквенный элемент'
        verbose_name_plural = 'Буквенные элементы'


# Пример: подпись; -/+; монограмма
class HwLinkNext(models.Model):
    link_next_id = models.AutoField(primary_key=True, editable=False)
    link_next = models.CharField(max_length=255)

    def __str__(self):
        return self.link_next

    class Meta:
        managed = True
        db_table = 'hw_link_next'
        verbose_name = 'Связь между буквами'
        verbose_name_plural = 'Связи между буквами'


# Пример: Форма движений при выполнении элементов начальная часть 1 элемент Р
class HwPartialSign(models.Model):
    partial_sign_id = models.AutoField(primary_key=True, editable=False)
    partial_sign_value = models.ForeignKey('HwPartialSignValue', models.DO_NOTHING, blank=True, null=True)
    partial_sign_value_concretization = models.ForeignKey('HwPartialSignValueConcretization', models.DO_NOTHING, blank=True, null=True)
    first_letter_concretization = models.ForeignKey('HwLetterConcretization', models.DO_NOTHING, blank=True, null=True, related_name='hw_partial_signs')
    second_letter_concretization = models.ForeignKey('HwLetterConcretization', models.DO_NOTHING, blank=True, null=True)
    relation_between_concretizations = models.IntegerField(blank=True, null=True)
    jsm_number = models.CharField(max_length=255, blank=True, null=True)
    general_jsm_number = models.CharField(max_length=255, blank=True, null=True)
    opt_general_jsm_number = models.CharField(max_length=255, blank=True, null=True)

    def get_sign_type_object(self):
        return self.partial_sign_value.partial_sign_type

    def get_type_number(self):
        ps_str = f'{self.partial_sign_value.partial_sign_type.partial_sign_type_number};' + str(
            self.first_letter_concretization.get_number())
        if self.second_letter_concretization:
            ps_str += '-' + str(self.second_letter_concretization.get_number()) + ';'
        return ps_str

    def get_sign_type_str(self):
        return f'{self.partial_sign_value.partial_sign_type}'

    def get_type_value_number(self):
        return f'{self.partial_sign_value.partial_sign_type.partial_sign_type_number};{self.partial_sign_value.partial_sign_value_number};'

    def get_partial_value(self):
        return f'(частн.)({self.get_type_value_number()}) {self.partial_sign_value.partial_sign_type}: {self.partial_sign_value}'

    def get_gen_partial_value(self):
        s = f'(обобщ.частн.)({self.general_jsm_number}) {self.partial_sign_value.partial_sign_type} {self.first_letter_concretization.letter_element.general_element}'
        if self.second_letter_concretization:
            s += f' и {self.second_letter_concretization.letter_element.general_element}'
        s += f' : {self.partial_sign_value.partial_sign_value}'
        if self.partial_sign_value_concretization:
            s += f' ({self.partial_sign_value_concretization})'

        return s

    def get_number(self):
        #  I; j:k1, k2; m1, m2
        '''
        I – номер группы, j – номер буквы (пары букв) или сама буква (пара букв),
        k1 – элемент буквы, k2 – конкретизация элемента буквы,
        m1 – значение признака, m2 – конкретизация значения.
        (Конкретизации элемента буквы и значения могут отсутствовать и тогда k2 и/или m2 равно 0).
        '''
        ps_str = f'{self.partial_sign_value.partial_sign_type.partial_sign_type_number}; ' + str(
            self.first_letter_concretization.get_number())
        if self.second_letter_concretization:
            ps_str += ' - ' + str(self.second_letter_concretization.get_number())

        ps_str += f'; {self.partial_sign_value.partial_sign_value_number}, '

        if self.partial_sign_value_concretization:
            ps_str += f'{self.partial_sign_value_concretization.partial_sign_value_concretization_number}'
        else:
            ps_str += '0'

        return ps_str

    def __str__(self):
        s = f'{self.partial_sign_value.partial_sign_type} {self.first_letter_concretization}'
        if self.second_letter_concretization:
            s += f' и {self.second_letter_concretization}'
        return s

    def get_str_pw(self):
        s = f'{self.partial_sign_value.partial_sign_type} {self.first_letter_concretization}'
        if self.second_letter_concretization:
            s += f' и {self.second_letter_concretization}'
        s += f' : {self.partial_sign_value.partial_sign_value}'
        if self.partial_sign_value_concretization:
            s += f' ({self.partial_sign_value_concretization})'
        return s

    def get_jsm_number(self):
        return f'({self.jsm_number})'

    def get_str_type(self):
        s = f'{self.partial_sign_value.partial_sign_type} {self.first_letter_concretization}'
        if self.second_letter_concretization:
            s += f' и {self.second_letter_concretization}'
        return s

    def get_str_value(self):
        s = f'{self.partial_sign_value.partial_sign_value}'
        if self.partial_sign_value_concretization:
            s += f' ({self.partial_sign_value_concretization})'
        return s

    def jsm_number_print_potential(self):
        return f'({self.general_jsm_number})'

    def jsm_value_print_potential(self):
        s = f'{self.partial_sign_value.partial_sign_type} {self.first_letter_concretization.letter_element.general_element}'
        if self.second_letter_concretization:
            s += f' и {self.second_letter_concretization.letter_element.general_element}'
        s += f' : {self.partial_sign_value.partial_sign_value}'
        if self.partial_sign_value_concretization:
            s += f' ({self.partial_sign_value_concretization})'
        return s


    class Meta:
        managed = True
        db_table = 'hw_partial_sign'


# Пример: Форма движений при выполнении соединения элементов
class HwPartialSignType(models.Model):
    partial_sign_type_id = models.AutoField(primary_key=True, editable=False)
    partial_sign_type = models.CharField(max_length=255)
    partial_sign_type_number = models.PositiveIntegerField(blank=True, null=True, editable=False)

    def __str__(self):
        return self.partial_sign_type


    class Meta:
        managed = True
        db_table = 'hw_partial_sign_type'
        verbose_name = 'Группа частных признаков'
        verbose_name_plural = 'Группы частных признаков'


# Пример: петлевая; угловатая; сверху вниз
class HwPartialSignValue(models.Model):
    partial_sign_value_id = models.AutoField(primary_key=True, editable=False)
    partial_sign_type = models.ForeignKey('HwPartialSignType', models.DO_NOTHING, blank=True, null=True)
    partial_sign_value = models.CharField(max_length=255, blank=True, null=True)
    partial_sign_value_number = models.PositiveIntegerField(blank=True, null=True, editable=False)

    def get_number(self):
        return f'{self.partial_sign_type.partial_sign_type_number};{self.partial_sign_value_number}'

    def get_dropdown_value(self):
        return self.partial_sign_value

    def __str__(self):
        return f'{self.partial_sign_value}'

    class Meta:
        managed = True
        db_table = 'hw_partial_sign_value'
        verbose_name = 'Значение частных признаков'
        verbose_name_plural = 'Значения частных признаков'
        ordering = ['partial_sign_type__partial_sign_type_number', 'partial_sign_value_number']


# Пример: за счет отсутствия надстрочного элемента
class HwPartialSignValueConcretization(models.Model):
    partial_sign_value_concretization_id = models.AutoField(primary_key=True, editable=False)
    partial_sign_value_concretization = models.CharField(max_length=255, blank=True, null=True)
    partial_sign_value_concretization_number = models.PositiveIntegerField(blank=True, null=True, editable=False)

    def __str__(self):
        return self.partial_sign_value_concretization

    def get_dropdown_value(self):
        return str(self.partial_sign_value_concretization)

    class Meta:
        managed = True
        db_table = 'hw_partial_sign_value_concretization'


# Пример: Форма движений при выполнении элементов пустая начальная часть росчерк и пустая вся буква дуговой элемент
class HwPartialWriting(models.Model):
    partial_writing_id = models.AutoField(primary_key=True, editable=False)
    first_letter_transcription = models.ForeignKey('HwTranscription', models.DO_NOTHING, blank=True, null=True, related_name='hw_partial_writings')
    second_letter_transcription = models.ForeignKey('HwTranscription', models.DO_NOTHING, blank=True, null=True)
    partial_sign = models.ForeignKey('HwPartialSign', models.DO_NOTHING, blank=True, null=True)
    amount_sign = models.PositiveIntegerField(blank=True, null=True)
    amount_all = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.partial_sign}'

    def get_set(self):
        return self.first_letter_transcription.set.table_str()

    class Meta:
        managed = True
        db_table = 'hw_partial_writing'


# Пример: Почкулев Д. А., подпись, количество: 9, достоверность: 0
class HwSet(models.Model):
    set_id = models.AutoField(primary_key=True, editable=False)
    person = models.ForeignKey('Person', models.DO_NOTHING, blank=True, null=True)
    type = models.ForeignKey('HwType', models.DO_NOTHING, blank=True, null=True)
    amount = models.PositiveIntegerField(blank=True, null=True)
    verity = models.IntegerField(blank=True, null=True)
    test_number = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        if self.test_number == '0' or self.test_number == '':
            return f'{self.person}, {self.type}, количество: {self.amount}, достоверность: {self.verity}'
        else:
            return f'{self.person}, тестовый образец № {self.test_number}'

    def table_str(self):
        if self.test_number == '0' or self.test_number == '':
            if self.verity == 1:
                return 'ДОСТОВЕРНОЕ МНОЖЕСТВО'
            else:
                return 'ПОДДЕЛЬНОЕ МНОЖЕСТВО'
        else:
            return upper(f'тестовый образец № {self.test_number}')

    def get_dropdown_value(self):
        return f'Тестовый образец № {self.test_number}'

    class Meta:
        managed = True
        db_table = 'hw_set'
        verbose_name = 'Набор подписей/расшифровок'
        verbose_name_plural = 'Наборы подписей/расшифровок'


# Пример: дуговой элемент +
class HwTranscription(models.Model):
    transcription_id = models.AutoField(primary_key=True, editable=False)
    set = models.ForeignKey(HwSet, models.DO_NOTHING, blank=True, null=True)
    letter_place = models.PositiveIntegerField(blank=True, null=True)
    link_next = models.ForeignKey('HwLinkNext', models.DO_NOTHING, blank=True, null=True)
    letter = models.ForeignKey('HwLetter', models.DO_NOTHING, blank=True, null=True)
    variant = models.IntegerField(blank=True, null=True)

    def str_for_form(self):
        return f'{self.letter} (место {self.letter_place})'

    def __str__(self):
        tr_str = str(self.letter) + ' '
        if self.variant:
            tr_str += '(в варианте) '

        if str(self.link_next) == 'последняя буква':
            return tr_str
        elif str(self.link_next) == 'подпись':
            return ''
        else:
            return tr_str + f'{self.link_next} '

    class Meta:
        managed = True
        db_table = 'hw_transcription'
        verbose_name = 'Транскрипция'
        verbose_name_plural = 'Транскрипции'


# Пример: расшифровка; подпись
class HwType(models.Model):
    type_id = models.AutoField(primary_key=True, editable=False)
    type = models.CharField(max_length=255)

    def __str__(self):
        return self.type

    class Meta:
        managed = True
        db_table = 'hw_type'
        verbose_name = 'Тип набора'
        verbose_name_plural = 'Типы наборов'


# нет примера в админке
class HwGeneralElement(models.Model):
    general_element_id = models.AutoField(primary_key=True, editable=False)
    general_element = models.CharField(max_length=255)
    general_element_number = models.PositiveIntegerField(editable=False)

    def __str__(self):
        return self.general_element

    class Meta:
        managed = True
        db_table = 'hw_general_element'


SEX_VALUES = (
    ('м', 'м'),
    ('ж', 'ж')
)


# Пример: Брюквин Я. А.
class Person(models.Model):
    person_id = models.AutoField(primary_key=True, editable=False)
    person_name = models.CharField(max_length=255, blank=True, null=True)
    person_surname = models.CharField(max_length=255, blank=True, null=True)
    person_patronymic = models.CharField(max_length=255, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    position = models.ForeignKey('Position', models.DO_NOTHING, blank=True, null=True)
    sex = models.CharField(max_length=2, blank=True, null=True, choices=SEX_VALUES)
    person_number = models.PositiveIntegerField(blank=True, null=True, editable=False)

    def __str__(self):
        s = f'{self.person_surname} {self.person_name[0]}.'
        if self.person_patronymic:
            s += f' {self.person_patronymic[0]}.'
        return s

    def jsm_number_print(self):
        return f'({self.person_number})'

    def jsm_dupl_print(self):
        return ''

    def jsm_value_print(self):
        s = f'{self.person_surname} {self.person_name[0]}.'
        if self.person_patronymic:
            s += f' {self.person_patronymic[0]}.'
        return s

    def jsm_number_print_potential(self):
        return self.jsm_number_print()

    def jsm_value_print_potential(self):
        return self.jsm_value_print()

    def    snippet(self):
        return f'{self.person_surname} {self.person_name[0]}. {self.person_patronymic[0]}.'

    class Meta:
        managed = True
        db_table = 'person'
        ordering = ['person_id']
        verbose_name = 'Респондент'
        verbose_name_plural = 'Респонденты'


class Position(models.Model):
    position_id = models.AutoField(primary_key=True, editable=False)
    position = models.CharField(max_length=255)

    def __str__(self):
        return self.position

    class Meta:
        managed = True
        db_table = 'position'
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'


# Пример: ОСТ | 8. В свободное время Вам всегда хочется заняться чем-либо? : 1. да (1 б.)
class QAnswer(models.Model):
    answer_id = models.AutoField(primary_key=True)
    question = models.ForeignKey('QQuestion', models.DO_NOTHING, blank=True, null=True)
    number_in_question = models.IntegerField(blank=True, null=True)
    answer_text = models.ForeignKey('QAnswerText', models.DO_NOTHING, blank=True, null=True)
    score = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.question}: {self.number_in_question}. {self.answer_text} ({self.score} б.)'

    class Meta:
        managed = True
        db_table = 'q_answer'
        verbose_name = 'Связь ответ-вопрос'
        verbose_name_plural = 'Связи ответ-вопрос'


# Пример: САН | 30. довольный-недовольный: 7. 3 (1 б.)
class QAnswerText(models.Model):
    answer_text_id = models.AutoField(primary_key=True)
    answer_text = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.answer_text

    class Meta:
        managed = True
        db_table = 'q_answer_text'
        verbose_name = 'Текст ответа'
        verbose_name_plural = 'Тексты ответов'


class QComment(models.Model):
    comment_id = models.AutoField(primary_key=True)
    comment_text = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.comment_text

    class Meta:
        managed = True
        db_table = 'q_comment'
        verbose_name = 'Комментарий к разбиению'
        verbose_name_plural = 'Комментарии к разбиениям'


# Пример: вв (высокий-высокий)
class QIntervalName(models.Model):
    interval_name_id = models.AutoField(primary_key=True)
    interval_nickname = models.CharField(max_length=255, blank=True, null=True)
    interval_name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.interval_nickname} ({self.interval_name})'

    class Meta:
        managed = True
        db_table = 'q_interval_name'
        verbose_name = 'Название словесного интервала разбиения'
        verbose_name_plural = 'Названия словесных интервалов разбиений'


# Пример: 9-12
class QIntervalScore(models.Model):
    interval_score_id = models.AutoField(primary_key=True)
    min_score = models.IntegerField(blank=True, null=True)
    max_score = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.min_score} - {self.max_score}'

    class Meta:
        managed = True
        db_table = 'q_interval_score'
        verbose_name = 'Числовой интервал разбиения'
        verbose_name_plural = 'Числовые интервалы разбиений'


# Пример: нн-вв-9 | 8. вс (высокий-средний)
class QIntervalSet(models.Model):
    interval_set_id = models.AutoField(primary_key=True)
    interval_set_name = models.ForeignKey('QIntervalSetName', models.DO_NOTHING, blank=True, null=True)
    interval_name = models.ForeignKey('QIntervalName', models.DO_NOTHING, blank=True, null=True)
    number_in_interval_set = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.interval_set_name} | {self.number_in_interval_set}. {self.interval_name}'

    def snippet(self):
        return {self.interval_name}

    class Meta:
        managed = True
        db_table = 'q_interval_set'
        verbose_name = 'Набор словесных интервалов разбиения'
        verbose_name_plural = 'Наборы словесных интервалов разбиений'


# Пример: нн-вв-9/стены/контроль
class QIntervalSetName(models.Model):
    interval_set_name_id = models.AutoField(primary_key=True)
    interval_set_name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.interval_set_name

    class Meta:
        managed = True
        db_table = 'q_interval_set_name'
        verbose_name = 'Название набора словесных интервалов'
        verbose_name_plural = 'Названия наборов словесных интервалов'

# Пример: 3. 9 - 12 | Стандартное | Социальная эмоциональность - н-в-3 | 3. в (высокий)
class QPartitioning(models.Model): #TODO: нет связи в локальном mysql, на сервере есть!! (int_score)
    partitioning_id = models.AutoField(primary_key=True)
    interval_score = models.ForeignKey('QIntervalScore', models.DO_NOTHING, blank=True, null=True)
    partitioning_scale = models.ForeignKey('QPartitioningScale', models.DO_NOTHING, blank=True, null=True)
    number_in_partitioning = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.number_in_partitioning}. {self.interval_score} | {self.partitioning_scale}'

    class Meta:
        managed = True
        db_table = 'q_partitioning'
        verbose_name = 'Набор числовых интервалов разбиения'
        verbose_name_plural = 'Наборы числовых интервалов разбиений'


# Пример: Дробное | Тревожность - нн-вв-9 | 9. вв (высокий-высокий)
class QPartitioningScale(models.Model):
    partitioning_scale_id = models.AutoField(primary_key=True)
    scale = models.ForeignKey('QScale', models.DO_NOTHING, blank=True, null=True)
    partitioning_scale_name = models.ForeignKey('QPartitioningScaleName', models.DO_NOTHING, blank=True, null=True)
    interval_set = models.ForeignKey('QIntervalSet', models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.partitioning_scale_name} | {self.scale} - {self.interval_set}'

    class Meta:
        managed = True
        db_table = 'q_partitioning_scale'
        verbose_name = 'Связь разбиение-шкала'
        verbose_name_plural = 'Связи разбиение-шкала'


# Стандартное/дробное
class QPartitioningScaleName(models.Model):
    partitioning_scale_name_id = models.AutoField(primary_key=True)
    partitioning_scale_name = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.partitioning_scale_name

    class Meta:
        managed = True
        db_table = 'q_partitioning_scale_name'
        verbose_name = 'Название разбиения шкалы'
        verbose_name_plural = 'Названия разбиений шкал'


# Пример: Охлупин Н. А.	БП | 24. Иногда я настолько выходил из себя, что ломал вещи: 1. полностью не согласен (1 б.)
class QPersonSelection(models.Model):
    person_selection_id = models.AutoField(primary_key=True)
    person = models.ForeignKey('Person', models.DO_NOTHING, blank=True, null=True)
    answer = models.ForeignKey('QAnswer', models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.person}: {self.answer}'

    class Meta:
        managed = True
        db_table = 'q_person_selection'
        verbose_name = 'Выбор респондента'
        verbose_name_plural = 'Выборы респондентов'


# Пример: 1	ОСТ	Подвижный ли Вы человек?
class QQuestion(models.Model):
    question_id = models.AutoField(primary_key=True)
    number_in_questionnaire = models.IntegerField(blank=True, null=True)
    questionnaire = models.ForeignKey('QQuestionnaire', models.DO_NOTHING, blank=True, null=True)
    question_text = models.ForeignKey('QQuestionText', models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.questionnaire} | {self.number_in_questionnaire}. {self.question_text}'

    class Meta:
        managed = True
        db_table = 'q_question'
        ordering = ['question_id']
        verbose_name = 'Связь вопрос-опросник'
        verbose_name_plural = 'Связи вопрос-опросник'


# Пример: Враждебность	БП | 23. Если человек слишком мил со мной, значит он от меня что-то хочет
class QQuestionScale(models.Model):
    question_scale_id = models.AutoField(primary_key=True)
    question = models.ForeignKey('QQuestion', models.DO_NOTHING, blank=True, null=True)
    scale = models.ForeignKey('QScale', models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.scale} | {self.question}'

    class Meta:
        managed = True
        db_table = 'q_question_scale'
        verbose_name = 'Связь вопрос-шкала'
        verbose_name_plural = 'Связи вопрос-шкала'


class QQuestionText(models.Model):
    question_text_id = models.AutoField(primary_key=True)
    question_text = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.question_text

    class Meta:
        managed = True
        db_table = 'q_question_text'
        ordering = ['question_text_id']
        verbose_name = 'Текст вопроса'
        verbose_name_plural = 'Тексты вопросов'


# ОСТ
class QQuestionnaire(models.Model):
    questionnaire_id = models.AutoField(primary_key=True)
    questionnaire_nickname = models.CharField(max_length=255, blank=True, null=True)
    questionnaire_name = models.CharField(max_length=255, blank=True, null=True)
    questionnaire_description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.questionnaire_nickname

    def snippet(self):
        return self.questionnaire_name

    class Meta:
        managed = True
        db_table = 'q_questionnaire'
        ordering = ['questionnaire_id']
        verbose_name = 'Название опросника'
        verbose_name_plural = 'Названия опросников'


# Пример: Охлупин Н. А.: 10 баллов по шкале Враждебность
class QResult(models.Model):
    result_id = models.AutoField(primary_key=True)
    person = models.ForeignKey('Person', models.DO_NOTHING, blank=True, null=True)
    scale = models.ForeignKey('QScale', models.DO_NOTHING, blank=True, null=True)
    raw_score = models.IntegerField(blank=True, null=True)
    result_score = models.IntegerField(blank=True, null=True)
    # partitioning = models.ForeignKey('QPartitioning', models.DO_NOTHING, blank=True, null=True)

    def __str__(self):
        return f'{self.person}: {self.result_score} баллов по шкале {self.scale}'

    class Meta:
        managed = True
        db_table = 'q_result'
        verbose_name = 'Результат по шкале'
        verbose_name_plural = 'Результаты по шкалам'


# Пример: Враждебность
class QScale(models.Model):
    scale_id = models.AutoField(primary_key=True)
    scale_name = models.CharField(max_length=255, blank=True, null=True)
    scale_nickname = models.CharField(max_length=255, blank=True, null=True)
    number_in_result = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.scale_name

    class Meta:
        managed = True
        db_table = 'q_scale'
        verbose_name = 'Название шкалы опросника'
        verbose_name_plural = 'Названия шкал опросников'
