from django.core.management.base import BaseCommand

from main.models import QScale, QPartitioningScale,QIntervalSet


# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         scales = QScale.objects.all().exclude(scale_id=1)
#         intsets = QIntervalSet.objects.filter(interval_set_name_id=2)
#         for scale in scales:
#             for intset in intsets:
#                 QPartitioningScale.objects.create(
#                     scale_id=scale.scale_id,
#                     interval_set_id=intset.interval_set_id,
#                     partitioning_scale_name='Вариант Гусаковой'
#                 )

# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         scales = QScale.objects.all()
#         intsets = QIntervalSet.objects.filter(interval_set_name_id=1)
#         for scale in scales:
#             for intset in intsets:
#                 QPartitioningScale.objects.create(
#                     scale_id=scale.scale_id,
#                     interval_set_id=intset.interval_set_id,
#                     partitioning_scale_name='Стандартное'
#                 )

# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(partitioning_scale_id__range=(151, 225))
#         for partscale in partscales:
#             partscale.partitioning_scale_name_id = 3
#             partscale.save()

# ОЧХ стены
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         scales = QScale.objects.filter(scale_id__in=[8,9,10,11,12,13,14,15,16,17])
#         intsets = QIntervalSet.objects.filter(interval_set_name_id=5)
#         for scale in scales:
#             for intset in intsets:
#                 QPartitioningScale.objects.create(
#                     scale_id=scale.scale_id,
#                     interval_set_id=intset.interval_set_id,
#                     partitioning_scale_name_id=3
#                 )

# ОЧХ стены стандартное
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         scales = QScale.objects.filter(scale_id__in=[8,9,10,11,12,13,14,15,16,17])
#         intsets = QIntervalSet.objects.filter(interval_set_name_id=5)
#         for scale in scales:
#             for intset in intsets:
#                 QPartitioningScale.objects.create(
#                     scale_id=scale.scale_id,
#                     interval_set_id=intset.interval_set_id,
#                     partitioning_scale_name_id=3
#                 )