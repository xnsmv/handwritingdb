from django.core.management.base import BaseCommand

from main.models import *

#ОСТ - проблема в 96 вопросе (он 2 раза) и 98 (его нигде нет)
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         questions = QQuestionScale.objects.filter(scale__in=[1,6]).values_list('question', flat=True).order_by('scale')
#         answers = QAnswer.objects.filter(question__questionnaire=1, question__in=questions)
#         for ans in answers:
#             print(ans)

#ОЧХ
class Command(BaseCommand):
    def handle(self, *args, **options):
        questions = QQuestionScale.objects.filter(scale__in=[21]).values_list('question', flat=True).order_by('scale')
        answers = QAnswer.objects.filter(question__questionnaire=4, question__in=questions)
        for ans in answers:
            print(ans)