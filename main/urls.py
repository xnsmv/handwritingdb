from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.home_page, name='home_page'),
    path('register/', views.register, name='register'),
    path('login/', views.login_user, name='login'),
    path('logout/', views.logout_user, name="logout"),

    # QSTNRS
    path('tests/', views.tests, name='tests'),
    path('tests/overview', views.tests_overview, name='tests_overview'),
    path('tests/<int:person_id>/<int:questionnaire_id>', views.person_test, name='person_test'),
    path('tests/subbase', views.persons_with_tests_and_signature, name='person_subbase'),


    # JSM HW
    path('hw/jsm/', views.jsm, name='jsm'),
    path('hw/jsm/trans/', views.jsm_trans, name='jsm_trans'),
    path('hw/jsm/trans/<int:indx>/ps', views.jsm_ps, name='jsm_ps'),
    path('hw/jsm/trans/<int:indx>/ps', views.jsm_ps_minus, name='jsm_ps_minus'),

    # JSM QSTNRS
    path('research/similarities_q/', views.similarities_q_view, name='similarities_q'),
    path('research/similarities_gw/', views.similarities_gw_view, name='similarities_gw'),
    path('research/similarities_general/', views.similarities_general_view, name='similarities_general'),
    path('research/jsm_choose_option/', views.jsm_choose_option_view, name='jsm_choose_option'),
    path(r'research/potential_hypotheses/', views.potential_hypotheses_view, name='potential_hypotheses'),

    # HW GEN
    path('hw/', views.person_list, name='person_list'),
    url(r'^hw/person/(?P<pk>\d+)[/|]$', views.PersonDetailView.as_view(), name='person_detail'),
    path('hw/person/add_person/', views.create_person, name='create_person'),
    path('hw/person/edit_person/<int:id>/', views.edit_person, name='edit_person'),
    path('hw/person/delete_person/<int:id>/', views.delete_person, name='delete_person'),
    path(r'hw/person/<int:person_id>/add_set/', views.create_set, name='create_set'),
    path(r'hw/person/<int:person_id>/delete_set/<int:id>/', views.delete_set, name='delete_set'),
    path(r'hw/person/<int:person_id>/edit_set/<int:id>/', views.edit_set, name='edit_set'),
    path(r'hw/person/<int:person_id>/general_writing/', views.general_writing, name='general_writing'),
    path(r'hw/person/<int:person_id>/general_writing/delete/<int:writing_id>',
         views.delete_general_writing, name='delete_general_writing'),

    path('ajax/load-gen-values/', views.load_values, name='ajax_load_values'),
    path(r'general_writing/gen_sign_type/', views.gen_sign_type, name='gen_sign_type'),
    path(r'general_writing/gen_sign_type/delete/<int:type_id>', views.delete_gen_sign_type,
         name='delete_gen_sign_type'),
    path(r'general_writing/gen_sign_type/edit/<int:type_id>', views.edit_gen_sign_type, name='edit_gen_sign_type'),

    path(r'general_writing/gen_sign_type/<int:type_id>/gen_sign_value/',
         views.gen_sign_value, name='gen_sign_value'),
    path(r'general_writing/gen_sign_type/<int:type_id>/gen_sign_value/delete/<int:value_id>',
         views.delete_gen_sign_value, name='delete_gen_sign_value'),

    # HW PARTIAL
    path(r'hw/person/<int:person_id>/partial_writing/', views.partial_writing, name='partial_writing'),
    path(r'hw/person/<int:person_id>/partial_writing_v2/', views.partial_writing_v2, name='partial_writing_v2'),
    path(r'hw/person/<int:person_id>/partial_writing/delete/<int:writing_id>',
         views.delete_partial_writing, name='delete_partial_writing'),
    path('ajax/load-letters/', views.load_letters, name='ajax_load_letters'),
    path('ajax/load-partial-values/', views.load_part_values, name='ajax_load_part_values'),
    path('ajax/load-partial-value-concretizations/',
         views.load_part_value_concretizations,
         name='ajax_load_part_value_concretizations'),
    path('ajax/load-part-fle/', views.load_part_fle, name='ajax_load_part_fle'),
    path('ajax/load-part-sle/', views.load_part_sle, name='ajax_load_part_sle'),
    path('ajax/load-part-flec/', views.load_part_flec, name='ajax_load_part_flec'),
    path('ajax/load-part-slec/', views.load_part_slec, name='ajax_load_part_slec'),

    path(r'hw/person/<int:person_id>/partial_writing/transcription', views.create_transcription, name='transcription'),
    path('ajax/load-transcription/', views.load_transcription, name='ajax_load_transcription'),
    path(r'hw/person/<int:person_id>/partial_writing/transcription/delete/<int:set_id>', views.delete_transcription),

    # PART SIGN
    path(r'hw/partial_writing/part_sign/', views.part_sign, name='part_sign'),
    path(r'hw/partial_writing/part_sign/delete/<int:sign_id>', views.delete_part_sign, name='delete_part_sign'),
    # PART TYPE
    path(r'hw/partial_writing/part_sign_type/', views.part_sign_type, name='part_sign_type'),
    path(r'hw/person/<int:person_id>/partial_writing/part_sign_type/', views.part_sign_type,
         name='part_sign_type_person'),
    path(r'hw/partial_writing/part_sign_type/delete/<int:type_id>', views.delete_part_sign_type,
         name='delete_part_sign_type'),

    # PART VALUE
    path(r'hw/partial_writing/part_sign_type/<int:type_id>/part_sign_value/', views.part_sign_value,
         name='part_sign_value'),
    path(r'hw/person/<int:person_id>/partial_writing/part_sign_type/<int:type_id>/part_sign_value/',
         views.part_sign_value, name='part_sign_value_person'),
    path(r'hw/partial_writing/part_sign_type/<int:type_id>/part_sign_value/delete/<int:value_id>',
         views.delete_part_sign_value, name='delete_part_sign_value'),

    # PART VALUE CONCRETIZATION
    path(r'hw/person/<int:person_id>/partial_writing/part_sign_type/<int:type_id>/part_sign_value_concretization/',
         views.part_sign_value_concretization, name='part_sign_value_concretization'),
    path(r'hw/partial_writing/part_sign_type/<int:type_id>/part_sign_value_concretization/',
         views.part_sign_value_concretization, name='part_sign_value_concretization'),
    path(r'hw/partial_writing/part_sign_value_concretization/<int:concr_id>/delete/',
         views.delete_part_sign_value_concretization, name='delete_part_sign_value_concretization'),

    # LETTER
    path(r'hw/letter/', views.letter, name='letter'),
    path(r'hw/letter/delete/<int:letter_id>', views.delete_letter, name='delete_letter'),

    # LETTER ELEMENT
    path(r'hw/letter/<int:letter_id>/letter_element', views.letter_element, name='letter_element'),
    path(r'hw/letter/<int:letter_id>/letter_element/delete/<int:letter_element_id>', views.delete_letter_element,
         name='delete_letter_element'),

    # LETTER ELEMENT CONCRETIZATION
    path(r'hw/person/<int:person_id>/partial_writing/<int:letter_element_id>/letter_concretization',
         views.letter_concretization, name='letter_concretization'),
    path(r'hw/letter/<int:letter_id>/letter_element/<int:letter_element_id>/letter_concretization',
         views.letter_concretization, name='letter_concretization'),
    path(r'hw/partial_writing/concretization/<int:concretization_id>/delete',
         views.delete_concretization, name='delete_concretization'),
    path(r'hw/partial_writing/<int:letter_element_id>/<int:concretization_id>/delete',
         views.delete_letter_concretization, name='delete_letter_concretization'),

    # IDENTIFICATION
    path(r'hw/signature_identification/', views.signature_identification, name='signature_identification'),
    path('ajax/load-tests/', views.load_tests, name='ajax_load_tests'),


    # informativeness research
    path(r'hw/informativeness_research/', views.informativeness_research, name='informativeness_research'),
    path(r'hw/group_signs/', views.group_signs, name='group_signs'),
]
