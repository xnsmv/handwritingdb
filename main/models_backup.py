from django.db import models


SEX_VALUES = (
    ('м', 'м'),
    ('ж', 'ж')
)


class Person(models.Model):
    person_name = models.CharField(max_length=100)
    person_surname = models.CharField(max_length=100)
    person_patronymic = models.CharField(max_length=100)
    birthday = models.DateField()
    sex = models.CharField(max_length=2, choices=SEX_VALUES)
    position = models.ForeignKey('main.Position', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.person_surname} {self.person_name[0]}. {self.person_patronymic[0]}.'

    class Meta:
        db_table = "person"
        managed = False


class Position(models.Model):
    position = models.CharField(max_length=100)

    def __str__(self):
        return self.position

    class Meta:
        db_table = "position"
        managed = False


class Set(models.Model):
    person = models.ForeignKey('main.Person', null=True, blank=True, on_delete=models.SET_NULL)
    type = models.ForeignKey('main.Type', null=True, blank=True, on_delete=models.SET_NULL)
    amount = models.IntegerField()
    verity = models.BooleanField()
    test_number = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.person}, {self.type}, количество: {self.amount}'

    class Meta:
        db_table = "hw_set"
        managed = False


class Type(models.Model):
    type = models.CharField(max_length=100)

    def __str__(self):
        return self.type

    class Meta:
        db_table = "hw_type"
        managed = False


class Image(models.Model):
    set = models.ForeignKey('main.Set', null=True, blank=True, on_delete=models.SET_NULL)
    image_path = models.CharField(max_length=100)  # TODO: подумать как картинки хранить
    image = models.ImageField(null=True)

    def __str__(self):
        return self.image  # TODO: придумать вывод

    class Meta:
        db_table = "hw_image"
        managed = False


class GeneralSignType(models.Model):
    general_sign_type = models.CharField(max_length=500)
    general_sign_type_number = models.PositiveIntegerField()

    def __str__(self):
        return self.general_sign_type

    class Meta:
        db_table = "hw_general_sign_type"
        managed = False


class GeneralSignValue(models.Model):
    general_sign_type = models.ForeignKey('main.GeneralSignType', null=True, blank=True, on_delete=models.SET_NULL)
    general_sign_value = models.CharField(max_length=500)
    general_sign_value_number = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.general_sign_type}: {self.general_sign_value}'

    class Meta:
        db_table = "hw_general_sign_value"
        managed = False


class GeneralWriting(models.Model):
    set = models.ForeignKey('main.Set', null=True, blank=True, on_delete=models.SET_NULL)
    comment = models.CharField(max_length=2000)
    general_sign_value = models.ForeignKey('main.GeneralSignValue', null=True, blank=True, on_delete=models.SET_NULL)
    amount_sign = models.PositiveIntegerField()
    amount_all = models.PositiveIntegerField()

    def __str__(self):
        return self.amount_all  # TODO: придумать вывод

    class Meta:
        db_table = "hw_general_writing"
        managed = False


class LinkNext(models.Model):
    link_next = models.CharField(max_length=10)

    def __str__(self):
        return self.link_next

    class Meta:
        db_table = "hw_link_next"
        managed = False


class Transcription(models.Model):
    set = models.ForeignKey('main.Set', null=True, blank=True, on_delete=models.SET_NULL)
    letter_place = models.PositiveIntegerField()
    link_next = models.ForeignKey('main.LinkNext', null=True, blank=True, on_delete=models.SET_NULL)
    letter = models.ForeignKey('main.Letter', null=True, blank=True, on_delete=models.SET_NULL)
    variant = models.BooleanField()

    def __str__(self):
        return f'{self.set}: {self.letter} в позиции {self.letter_place}'

    class Meta:
        db_table = "hw_transcription"
        managed = False


class Letter(models.Model):
    letter = models.CharField(max_length=3)
    letter_number = models.PositiveIntegerField()

    def __str__(self):
        return self.letter

    class Meta:
        db_table = "hw_letter"
        managed = False


class Element(models.Model):
    element = models.CharField(max_length=100)
    element_number = models.PositiveIntegerField()

    def __str__(self):
        return self.element

    class Meta:
        db_table = "hw_element"
        managed = False


class LetterElement(models.Model):
    letter = models.ForeignKey('main.Letter', null=True, blank=True, on_delete=models.SET_NULL)
    element = models.ForeignKey('main.Element', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.element} буквы {self.letter}'

    class Meta:
        db_table = "hw_letter_element"
        managed = False


class Concretization(models.Model):
    concretization = models.CharField(max_length=100)
    concretization_number = models.PositiveIntegerField()

    def __str__(self):
        return self.concretization

    class Meta:
        db_table = "person"
        managed = False


class LetterConcretization(models.Model):
    letter_element = models.ForeignKey('main.LetterElement', null=True, blank=True, on_delete=models.SET_NULL)
    concretization = models.ForeignKey('main.Concretization', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.letter_element}: {self.concretization}'

    class Meta:
        db_table = "hw_letter_concretization"
        managed = False


class PartialSignType(models.Model):
    partial_sign_type = models.CharField(max_length=300)
    partial_sign_number = models.PositiveIntegerField()

    def __str__(self):
        return self.partial_sign_type

    class Meta:
        db_table = "hw_partial_sign_type"
        managed = False


class PartialSignValue(models.Model):
    partial_sign_type = models.ForeignKey('main.PartialSignType', null=True, blank=True, on_delete=models.SET_NULL)
    partial_sign_value = models.CharField(max_length=300)
    partial_sign_value_number = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.partial_sign_type}: {self.partial_sign_value}'

    class Meta:
        db_table = "hw_partial_sign_value"
        managed = False

class PartialSignValueConcretization(models.Model):
    partial_sign_value_concretization = models.CharField(max_length=300)
    partial_sign_value_concretization_number = models.PositiveIntegerField()

    def __str__(self):
        return self.partial_sign_value_concretization

    class Meta:
        db_table = "hw_partial_sign_value_concretization"
        managed = False


class PartialSign(models.Model):
    partial_sign_value = models.ForeignKey('main.PartialSignValue', null=True, blank=True, on_delete=models.SET_NULL)
    partial_sign_value_concretization = models.ForeignKey('main.PartialSignValueConcretization', null=True, blank=True, on_delete=models.SET_NULL)
    first_letter_concretization = models.ForeignKey('main.LetterConcretization', null=True, blank=True, on_delete=models.SET_NULL, related_name='first_letter_concr')
    second_letter_concretization = models.ForeignKey('main.LetterConcretization', null=True, blank=True, on_delete=models.SET_NULL, related_name='second_letter_concr')
    relation_between_concretizations = models.CharField(max_length=100)

    def __str__(self):
        return self.first_letter_concretization  # TODO: придумать вывод

    class Meta:
        db_table = "hw_partial_sign"
        managed = False


class PartialWriting(models.Model):
    first_letter_transcription = models.ForeignKey('main.Transcription', null=True, blank=True, on_delete=models.SET_NULL, related_name='first_letter_tr')
    second_letter_transcription = models.ForeignKey('main.Transcription', null=True, blank=True, on_delete=models.SET_NULL, related_name='second_letter_tr')
    partial_sign = models.ForeignKey('main.PartialSign', null=True, blank=True, on_delete=models.SET_NULL)
    amount_sign = models.PositiveIntegerField()
    amount_all = models.PositiveIntegerField()

    def __str__(self):
        return ''

    class Meta:
        db_table = "hw_partial_writing"
        managed = False


class Answer(models.Model):
    question = models.ForeignKey('main.Question', null=True, blank=True, on_delete=models.SET_NULL)
    answer_text = models.ForeignKey('main.AnswerText', null=True, blank=True, on_delete=models.SET_NULL)
    number_in_question = models.PositiveIntegerField()
    score = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.number_in_question}. {self.answer_text}'

    class Meta:
        db_table = "q_answer"
        managed = False


class AnswerText(models.Model):
    answer_text = models.CharField(max_length=300)

    def __str__(self):
        return self.answer_text

    class Meta:
        db_table = "q_answer_text"
        managed = False


class Comment(models.Model):
    comment_text = models.CharField(max_length=500)

    def __str__(self):
        return self.comment_text

    class Meta:
        db_table = "q_comment"
        managed = False


class IntervalName(models.Model):
    interval_abbreviation = models.CharField(max_length=100)
    interval_name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.interval_abbreviation} ({self.interval_name})'

    class Meta:
        db_table = "q_interval_name"
        managed = False


class IntervalScore(models.Model):
    min_score = models.PositiveIntegerField()
    max_score = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.min_score} - {self.max_score}'

    class Meta:
        db_table = "q_interval_score"
        managed = False


class IntervalSet(models.Model):
    interval_set_name = models.ForeignKey('main.IntervalSetName', null=True, blank=True, on_delete=models.SET_NULL)
    interval_name = models.ForeignKey('main.IntervalName', null=True, blank=True, on_delete=models.SET_NULL)
    number_in_interval_set = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.number_in_interval_set}. {self.interval_name}'

    class Meta:
        db_table = "q_interval_set"
        managed = False


class IntervalSetName(models.Model):
    interval_set_name = models.CharField(max_length=100)

    def __str__(self):
        return self.interval_set_name

    class Meta:
        db_table = "q_interval_set_name"
        managed = False


class Partitioning(models.Model):
    interval_score = models.ForeignKey('main.IntervalScore', null=True, blank=True, on_delete=models.SET_NULL)
    partitioning_scale = models.ForeignKey('main.PartitioningScale', null=True, blank=True, on_delete=models.SET_NULL)
    number_in_partitioning = models.PositiveIntegerField()

    def __str__(self):  # TODO: Придумать вывод
        return self.number_in_partitioning

    class Meta:
        db_table = "q_partitioning"
        managed = False


class PartitioningScale(models.Model):
    scale = models.ForeignKey('main.Scale', null=True, blank=True, on_delete=models.SET_NULL)
    interval_set = models.ForeignKey('main.IntervalSet', null=True, blank=True, on_delete=models.SET_NULL)
    # чей вариант разбиения
    partitioning_scale_name = models.CharField(max_length=200)

    def __str__(self):
        return ''

    class Meta:
        db_table = "q_partitioning_scale"
        managed = False


class PersonSelection(models.Model):
    person = models.ForeignKey('main.Person', null=True, blank=True, on_delete=models.SET_NULL)
    answer = models.ForeignKey('main.Answer', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return ''

    class Meta:
        db_table = "q_person_selection"
        managed = False


class Question(models.Model):
    questionnaire = models.ForeignKey('main.Questionnaire', null=True, blank=True, on_delete=models.SET_NULL)
    question_text = models.ForeignKey('main.QuestionText', null=True, blank=True, on_delete=models.SET_NULL)
    number_in_questionnaire = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.questionnaire} | {self.number_in_questionnaire}. {self.question_text}'

    class Meta:
        db_table = "q_question"
        managed = False


class QuestionScale(models.Model):
    scale = models.ForeignKey('main.Scale', null=True, blank=True, on_delete=models.SET_NULL)
    question = models.ForeignKey('main.Question', null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return ''

    class Meta:
        db_table = "q_question_scale"
        managed = False


class QuestionText(models.Model):
    question_text = models.CharField(max_length=200)

    def __str__(self):
        return self.question_text

    class Meta:
        db_table = "q_question_text"
        managed = False


class Questionnaire(models.Model):
    questionnaire_name = models.CharField(max_length=200)
    questionnaire_abbreviation = models.CharField(max_length=50)

    def __str__(self):
        return ''

    class Meta:
        db_table = "q_questionnaire"
        managed = False


class Result(models.Model):
    person = models.ForeignKey('main.Person', null=True, blank=True, on_delete=models.SET_NULL)
    scale = models.ForeignKey('main.Scale', null=True, blank=True, on_delete=models.SET_NULL)
    raw_score = models.PositiveIntegerField()
    result_score = models.PositiveIntegerField()

    def __str__(self):
        return ''

    class Meta:
        db_table = "q_result"
        managed = False


class Scale(models.Model):
    scale_name = models.CharField(max_length=100)
    scale_abbreviation = models.CharField(max_length=100)

    def __str__(self):
        return ''

    class Meta:
        db_table = "q_scale"
        managed = False


# INSERT INTO q_questionnaire VALUES
# (1, 'ОСТ', 'Опросник структуры темперамента'),
# (2, 'ОЧХ', 'Опросник черт характера'),
# (3, 'САН', 'Самочувствие, активность, настроение'),
# (4, 'БП', 'Опросник склонности к агрессии Басса-Перри');
#
# --
# -- Вывод данных для таблицы q_answer_text
# --
# INSERT INTO q_answer_text VALUES
# (1, 'да'),
# (2, 'нет'),
# (3, 'полностью не согласен'),
# (4, 'скорее не согласен'),
# (5, 'согласен'),
# (6, 'полностью согласен');
