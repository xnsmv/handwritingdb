from django.forms import modelformset_factory
from django.urls import reverse
from django.views import generic

from django.db.models import Q

from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.db.models import Max, Sum
from django.shortcuts import render, redirect

from .decorators import unauthenticated_user, allowed_users, admin_only
from django.contrib.auth.decorators import login_required

from main.forms import *

import re


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def person_list(request):
    persons = Person.objects.order_by('person_surname', 'person_name')
    return render(request, 'hw/person_list.html', {'persons': persons,
                                                   'title': 'Список респондентов'})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def create_person(request):
    person_form = PersonForm()
    if request.method == "POST":
        person_form = PersonForm(request.POST)
        if person_form.is_valid():
            person = person_form.save(commit=False)
            person.save()
        else:
            return render(request, "hw/person.html", {'form': person_form})
    else:
        return render(request, "hw/person.html", {'form': person_form,
                                                  'title': 'Новый респондент'})
    return HttpResponseRedirect(reverse('person_list'))


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def edit_person(request, id):
    try:
        person = Person.objects.get(person_id=id)
        person_form = PersonForm(instance=person)
        if request.method == "POST":
            person_form = PersonForm(request.POST)
            if person_form.is_valid():
                person.person_surname = person_form.cleaned_data['person_surname']
                person.person_name = person_form.cleaned_data['person_name']
                person.person_patronymic = person_form.cleaned_data['person_patronymic']
                person.birthday = person_form.cleaned_data['birthday']
                person.position = person_form.cleaned_data['position']
                person.sex = person_form.cleaned_data['sex']
                person.save()
                return HttpResponseRedirect(reverse('person_list'))
            else:
                return render(request, "hw/person.html", {'form': person_form,
                                                          'title': 'Редактировать респондента'})
        else:
            return render(request, "hw/person.html", {'form': person_form,
                                                      'title': 'Редактировать респондента'})
    except Person.DoesNotExist:
        return HttpResponseNotFound("<h2>Person not found</h2>")


def delete_person(request, id):
    try:
        person = Person.objects.get(person_id=id)
        person.delete()
        return HttpResponseRedirect(reverse('person_list'))
    except Person.DoesNotExist:
        return HttpResponseNotFound("<h2>Person not found</h2>")


class PersonDetailView(generic.DetailView):
    model = Person

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        id = super().get_object().person_id
        context['title'] = Person.objects.get(person_id=id).__str__()

        scales = QResult.objects.filter(person=id).values_list('scale_id', flat=True)

        questions = QQuestionScale.objects.filter(scale_id__in=scales).values_list('question_id', flat=True)

        qstnrs_num = QQuestion.objects.filter(question_id__in=questions).values_list('questionnaire_id', flat=True).distinct()
        context['questionnaires'] = QQuestionnaire.objects.filter(questionnaire_id__in=qstnrs_num)
        context['id'] = id
        person_sets = HwSet.objects.filter(person_id__exact=id)
        if person_sets:
            context['sets'] = person_sets
            sets_with_general = HwGeneralWriting.objects.filter(set_id__in=person_sets.values('set_id'))

            true_set = HwSet.objects.filter(person_id__exact=id, verity__exact=1)
            if true_set:
                context['true_set'] = true_set
                gen_for_true_set = sets_with_general.filter(set_id__in=true_set.values('set_id')).select_related(
                    'general_sign_value')
                context['gen_for_true_set'] = gen_for_true_set

                false_set = HwSet.objects.filter(person_id__exact=id, verity__exact=0, test_number__exact=0)
                if false_set:
                    context['false_set'] = false_set

                    gen_for_false_set = sets_with_general.filter(set_id__in=false_set.values('set_id')).select_related(
                        'general_sign_value')
                    context['gen_for_false_set'] = gen_for_false_set
                    # context['sets_with_general'] = sets_with_general[0]
        return context


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def create_set(request, person_id):
    person = Person.objects.get(person_id=person_id)
    form = SetForm(person=person_id, verity=None)
    img_form = ImgForm(set=None)
    ImageFormSet = modelformset_factory(HwImage, form=ImgForm, extra=3)
    imgformset = ImageFormSet(queryset=HwImage.objects.none(), form_kwargs={'set': None})

    if request.method == "POST":
        imgformset = ImageFormSet(request.POST, request.FILES,
                                  queryset=HwImage.objects.none(),
                                  form_kwargs={'set': None})

        form = SetForm(request.POST, person=person_id, verity=None)
        if form.is_valid() and imgformset.is_valid():
            set = HwSet()
            set.person = Person.objects.get(person_id=person_id)
            set.amount = form.cleaned_data['amount']
            set.verity = form.clean_verity()
            set.type = form.cleaned_data['type']
            set.test_number = form.clean_testnumber()
            set.save()
            for form in imgformset.cleaned_data:
                if form:
                    image = form['image']
                    imageobj = HwImage(set=set, image=image)
                    imageobj.save()
            return HttpResponseRedirect(reverse('person_detail', args=[person_id]))

    return render(request, "hw/set.html", {'form': form,
                                           'person': person,
                                           'imgformset': imgformset,
                                           'title': 'Создать множество'})


def delete_set(request, person_id, id):
    try:
        set = HwSet.objects.get(set_id=id)
        set.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Set not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def edit_set(request, person_id, id):
    try:
        person = Person.objects.get(person_id=person_id)
        set = HwSet.objects.get(set_id=id)
        images = HwImage.objects.filter(set=set)
        form = SetForm(instance=set, person=person_id, verity=set.verity)
        ImageFormSet = modelformset_factory(HwImage, form=ImgForm, extra=3, can_delete=True)
        imgformset = ImageFormSet(queryset=HwImage.objects.filter(set=set), form_kwargs={'set': set})

        if request.method == "POST":
            form = SetForm(data=request.POST, person=person_id, verity=set.verity)
            imgformset = ImageFormSet(request.POST, request.FILES, form_kwargs={'set': set})
            if form.is_valid() and imgformset.is_valid():
                set.amount = form.cleaned_data['amount']
                set.verity = form.clean_verity()
                set.type = form.cleaned_data['type']
                set.test_number = form.clean_testnumber()
                set.save()
                imgformset.save()
            return HttpResponseRedirect(reverse('person_detail', args=[person_id]))

        return render(request, "hw/set.html", {'form': form,
                                               'person': person,
                                               'imgformset': imgformset,
                                               'images': images,
                                               'set': set,
                                               'title': 'Редактировать множество'})
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Set not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def general_writing(request, person_id):
    sets = HwSet.objects.filter(person_id__exact=person_id)
    try:
        first_set = HwSet.objects.get(set_id=request.GET.get("first_set"))
    except HwSet.DoesNotExist:
        first_set = None
    try:
        second_set = HwSet.objects.get(set_id=request.GET.get("second_set"))
    except HwSet.DoesNotExist:
        second_set = None

    if first_set:
        gen_for_first_set = HwGeneralWriting.objects.filter(set_id__exact=first_set.set_id)
    else:
        gen_for_first_set = HwGeneralWriting.objects.none()
    if second_set:
        gen_for_second_set = HwGeneralWriting.objects.filter(set_id__exact=second_set.set_id)
    else:
        gen_for_second_set = HwGeneralWriting.objects.none()

    gen_form = GenForm(person=person_id)
    if request.method == "POST":
        if 'saveModal' in request.POST:
            gen_writing = HwGeneralWriting.objects.get(general_writing_id=request.POST.get("writing_id"))
            gen_writing.amount_sign = request.POST.get("amount_sign")
            gen_writing.save()
        else:
            gen_writing = HwGeneralWriting()
            gen_writing.set = HwSet.objects.get(set_id=request.POST.get("set"))
            gen_value = HwGeneralSignValue.objects.get(general_sign_value_id=request.POST.get("general_sign_value"))
            gen_writing.general_sign_value = gen_value
            gen_writing.amount_all = first_set.amount
            # Не забыть проверку, чтобы amount_sign <= amount_all
            gen_writing.amount_sign = request.POST.get("amount")
            gen_writing.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        return render(request, "hw/general_writing.html", {
            'sets': sets,
            'true_set': first_set,
            'gen_for_true_set': gen_for_first_set,
            'false_set': second_set,
            'gen_for_false_set': gen_for_second_set,
            'gen_form': gen_form,
            #'prev_page': prev_page,
            'title': Person.objects.get(person_id=person_id).__str__() + ': общие признаки'
        })


def delete_general_writing(request, person_id, writing_id):
    try:
        gen_writing = HwGeneralWriting.objects.get(general_writing_id=writing_id)
        gen_writing.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwGeneralWriting.DoesNotExist:
        return HttpResponseNotFound("<h2>General writing not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def load_values(request):
    general_sign_type_id = request.GET.get('general_sign_type')
    values = HwGeneralSignValue.objects.filter(general_sign_type_id=general_sign_type_id)
    return render(request, 'hw/values_dropdown_list_options.html', {'values': values})


def load_tests(request):
    person_id = request.GET.get('person_id')
    values = HwSet.objects.filter(Q(person_id=person_id) & ~Q(test_number=0) & Q(test_number__isnull=False))
    return render(request, 'hw/values_dropdown_list_options.html', {'values': values})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def gen_sign_type(request):
    gen_types = HwGeneralSignType.objects.all()
    prev_page = ''
    if "person" in str(request.META.get('HTTP_REFERER', '/')):
        prev_page = request.META.get('HTTP_REFERER', '/')
    print(prev_page)
    if request.method == "POST":
        if 'saveModal' in request.POST:
            gen_type = HwGeneralSignType.objects.get(general_sign_type_id=request.POST.get("type_id"))
            gen_type.general_sign_type = request.POST.get("type")
            gen_type.save()
        else:
            gen_type = HwGeneralSignType()
            gen_type.general_sign_type = request.POST.get("general_sign_type")
            gen_type.general_sign_type_number = HwGeneralSignType.objects.aggregate(Max('general_sign_type_number'))[
                                                    'general_sign_type_number__max'] + 1
            gen_type.save()
        return HttpResponseRedirect(request.path_info)
    else:
        return render(request, 'hw/gen_sign_type.html', {"gen_types": gen_types, "prev_page": prev_page,
                                                         'title': 'Общие признаки'})


def delete_gen_sign_type(request, type_id):
    try:
        type = HwGeneralSignType.objects.get(general_sign_type_id=type_id)
        type.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>General Sign Type not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def edit_gen_sign_type(request, type_id):
    try:
        gen_types = HwGeneralSignType.objects.all()
        type = HwGeneralSignType.objects.get(general_sign_type_id=type_id)
        if request.method == "POST":
            type.general_sign_type = request.POST.get("general_sign_type")
            type.save()
            return HttpResponseRedirect(reverse('gen_sign_type'))
        else:
            return render(request, 'hw/gen_sign_type.html', {'gen_types': gen_types,
                                                             'gen_sign': type,
                                                             'title': 'Общие признаки'})
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>General Sign Type not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def gen_sign_value(request, type_id):
    prev_page = request.META.get('HTTP_REFERER', '/')
    gen_type = HwGeneralSignType.objects.get(general_sign_type_id=type_id)
    values = HwGeneralSignValue.objects.filter(general_sign_type_id__exact=type_id)
    if request.method == "POST":
        if 'saveModal' in request.POST:
            gen_val = HwGeneralSignValue.objects.get(general_sign_value_id=request.POST.get("value_id"))
            gen_val.general_sign_value = request.POST.get("value")
            gen_val.save()
        else:
            gen_val = HwGeneralSignValue()
            gen_val.general_sign_type_id = type_id
            gen_val.general_sign_value = request.POST.get("general_sign_value")
            # группа значения вычисляется в зависимости от группы признака
            gen_val.general_sign_value_number = \
                HwGeneralSignValue.objects.filter(general_sign_type_id__exact=type_id).aggregate(
                    Max('general_sign_value_number'))['general_sign_value_number__max'] + 1
            gen_val.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        return render(request, 'hw/gen_sign_value.html', {'type': gen_type, 'values': values,
                                                          'prev_page': prev_page,
                                                          'title': 'Изменить значения признака'})


def delete_gen_sign_value(request, type_id, value_id):
    try:
        val = HwGeneralSignValue.objects.get(general_sign_value_id=value_id)
        val.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>General Sign Value not found</h2>")


# HW PARTIAL

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def partial_writing(request, person_id):
    person = Person.objects.get(pk=person_id)
    sets = HwSet.objects.filter(person_id__exact=person_id)

    all_tran = HwTranscription.objects.filter(set_id__in=sets.values('set_id'))

    all_pw = HwPartialWriting.objects.filter(
        first_letter_transcription_id__in=all_tran.values('transcription_id'))
    all_ps = HwPartialSign.objects.filter(pk__in=all_pw.values('partial_sign_id'))

    form = PartForm(person=person_id)
    first_transcription = None
    second_transcription = None
    part_for_first_set = HwPartialWriting.objects.none()
    part_for_second_set = HwPartialWriting.objects.none()

    try:
        first_set = HwSet.objects.get(set_id=request.GET.get("first_set"))
    except HwSet.DoesNotExist:
        first_set = None
    try:
        second_set = HwSet.objects.get(set_id=request.GET.get("second_set"))
    except HwSet.DoesNotExist:
        second_set = None

    if first_set:
        first_transcription = HwTranscription.objects.filter(set_id__exact=first_set.set_id).order_by('letter_place')
        part_for_first_set = HwPartialWriting.objects.filter(
            first_letter_transcription_id__in=first_transcription.values('transcription_id'))

    if second_set:
        second_transcription = HwTranscription.objects.filter(set_id__exact=second_set.set_id).order_by('letter_place')
        part_for_second_set = HwPartialWriting.objects.filter(
            first_letter_transcription_id__in=second_transcription.values('transcription_id')).select_related()

    if request.method == "POST":
        if 'saveModal' in request.POST:
            part_writing = HwPartialWriting.objects.get(partial_writing_id=request.POST.get("writing_id"))
            part_writing.amount_sign = request.POST.get("amount_sign")
            part_writing.save()
        elif 'saveQuick' in request.POST:
            part_wr = HwPartialWriting.objects.get(pk=request.POST.get("all_pw"))
            set_id = request.POST.get("set")
            set_tran = HwTranscription.objects.filter(set_id=set_id)
            second_lt = None
            # first_lc = part_wr.first_letter_transcription.
            for tr in set_tran:
                print(part_wr.first_letter_transcription)
                if tr.letter == part_wr.first_letter_transcription.letter:
                    first_lt = tr
                elif part_wr.second_letter_transcription:
                    if tr.letter == part_wr.second_letter_transcription.letter:
                        second_lt = tr

            part_writing = HwPartialWriting()
            part_writing.partial_sign = part_wr.partial_sign
            part_writing.first_letter_transcription = first_lt
            if second_lt:
                part_writing.second_letter_transcription = second_lt
            else:
                part_writing.second_letter_transcription = None
            part_writing.save()
        else:
            part_writing = HwPartialWriting()
            form = PartForm(data=request.POST, person=person_id)
            if form.is_valid():
                print('VALID')
                first_letter = form.cleaned_data["first_letter"]
                second_letter = form.cleaned_data["second_letter"]
                partial_sign_value = form.cleaned_data["partial_sign_value"]
                partial_sign_value_concretization = form.cleaned_data["partial_sign_value_concretization"]
                print(partial_sign_value_concretization)
                first_letter_concretization = form.cleaned_data["first_letter_element_concretization"]
                second_letter_concretization = form.cleaned_data["second_letter_element_concretization"]
                relation_between_concretizations = form.clean_relation()
                amount = form.cleaned_data["amount"]
                set = form.cleaned_data["set"]

                part_sign_input = HwPartialSign.objects.filter(partial_sign_value=partial_sign_value,
                                                               first_letter_concretization=first_letter_concretization,
                                                               second_letter_concretization=second_letter_concretization,
                                                               partial_sign_value_concretization=partial_sign_value_concretization,
                                                               relation_between_concretizations=relation_between_concretizations)

                # if partial sign doesn't exist
                if not part_sign_input:
                    print('NOPE ')
                    partial_sign = HwPartialSign()
                    partial_sign.partial_sign_value = partial_sign_value
                    partial_sign.first_letter_concretization = first_letter_concretization
                    partial_sign.second_letter_concretization = second_letter_concretization
                    partial_sign.partial_sign_value_concretization = partial_sign_value_concretization
                    partial_sign.relation_between_concretizations = relation_between_concretizations
                    partial_sign.save()
                else:
                    partial_sign = part_sign_input.first()

                # print('NUMBER: ', partial_sign.get_number())
                # print('PS ID: ', partial_sign.pk)

                part_writing.partial_sign = partial_sign
                part_writing.first_letter_transcription = first_letter
                part_writing.second_letter_transcription = second_letter
                part_writing.amount_sign = amount
                part_writing.amount_all = set.amount
                part_writing.save()

    context = {'sets': sets,
               'true_set': first_set,
               'part_for_true_set': part_for_first_set,
               'false_set': second_set,
               'part_for_false_set': part_for_second_set,
               'gen_form': form,
               'true_transcription': first_transcription,
               'false_transcription': second_transcription,
               'person': person,
               'all_pw': all_pw,
               'title': Person.objects.get(person_id=person_id).__str__() + ': частные признаки'
               }
    return render(request, "hw/partial_writing.html", context)


def partial_writing_v2(request, person_id):
    for person in Person.objects.all():
        all_sets = HwSet.objects.filter(person=person)
        if len(all_sets) < 4 and len(all_sets) > 0:
            print('PERSON!!!!!', person)




    person = Person.objects.get(person_id=person_id)
    # все множества респондента
    all_sets = HwSet.objects.filter(person_id=person_id)
    # все транскрипции
    all_trans = HwTranscription.objects.filter(set__in=all_sets)
    # все признаки привязанные к транскрипциям
    all_pw = HwPartialWriting.objects.filter(
        Q(first_letter_transcription__in=all_trans) | Q(second_letter_transcription__in=all_trans))
    all_ps_types = set()
    for p in all_pw:
        all_ps_types.add(p.partial_sign)

    all_ps_types = list(all_ps_types)
    all_ps_types.sort(key=lambda i: i.__str__())

    if not all_ps_types:
        return HttpResponseNotFound("<h2>У данного респондента нет множеств или частных признаков</h2>")

    if len(all_sets) < 4:
        prev_sign = all_ps_types[0]
        table_fusion_2 = {}
        null_key = 0
        for ps in all_ps_types:

            if ps.get_type_number() != prev_sign.get_type_number():
                table_fusion_2[null_key] = []
                table_fusion_2[ps] = []
                null_key += 1
                prev_sign = ps
            else:
                table_fusion_2[ps] = []

        for p_set in all_sets:
            for ps in all_ps_types:
                curr_set = p_set
                set_trans = HwTranscription.objects.filter(set=curr_set)
                set_pw = HwPartialWriting.objects.filter(
                    ((Q(first_letter_transcription__in=set_trans) | Q(second_letter_transcription__in=set_trans)) \
                     & Q(partial_sign=ps))
                )
                if set_pw:
                    table_fusion_2[ps] += set_pw
                else:
                    print(table_fusion_2[ps])
                    table_fusion_2[ps].append(None)

        print(table_fusion_2)
        context = {
            'person': person,
            'all_pw': all_ps_types,
            'table_2': table_fusion_2,
            'all_sets': all_sets,
            'nulls': range(null_key)
        }
        print(range(null_key))
        print(range(10))
        print(null_key)
        return render(request, "hw/partial_writing_v2_new.html", context)


    true_set = HwSet.objects.get(verity=1, person=person)
    table_true = []
    for ps in all_ps_types:
        curr_set = true_set
        set_trans = HwTranscription.objects.filter(set=curr_set)
        set_pw = HwPartialWriting.objects.filter(
            (Q(first_letter_transcription__in=set_trans) | Q(second_letter_transcription__in=set_trans))
        )
        exact_pw = None
        for s in set_pw:
            if s.partial_sign == ps:
                exact_pw = s
                break
        if exact_pw:
            table_true.append(
                (ps, exact_pw)
            )
        else:
            table_true.append(
                (ps, None)
            )

    false_set = HwSet.objects.get(Q(person=person) & Q(verity=0) & Q(test_number=0) | Q(test_number__isnull=True))
    table_false = []

    for ps in all_ps_types:
        curr_set = false_set
        set_trans = HwTranscription.objects.filter(set=curr_set)
        set_pw = HwPartialWriting.objects.filter(
            (Q(first_letter_transcription__in=set_trans) | Q(second_letter_transcription__in=set_trans))
        )
        exact_pw = None
        for s in set_pw:
            if s.partial_sign == ps:
                exact_pw = s
                break
        if exact_pw:
            table_false.append(
                (ps, exact_pw)
            )
        else:
            table_false.append(
                (ps, None)
            )

    test1_set = HwSet.objects.get(Q(person=person) & Q(test_number='1'))
    table_test1 = []

    for ps in all_ps_types:
        curr_set = test1_set
        set_trans = HwTranscription.objects.filter(set=curr_set)
        set_pw = HwPartialWriting.objects.filter(
            (Q(first_letter_transcription__in=set_trans) | Q(second_letter_transcription__in=set_trans))
        )
        exact_pw = None
        for s in set_pw:
            if s.partial_sign == ps:
                exact_pw = s
                break
        if exact_pw:
            table_test1.append(
                (ps, exact_pw)
            )
        else:
            table_test1.append(
                (ps, None)
            )

    test2_set = HwSet.objects.get(Q(person=person) & Q(test_number='2'))
    table_test2 = []

    for ps in all_ps_types:
        curr_set = test2_set
        set_trans = HwTranscription.objects.filter(set=curr_set)
        set_pw = HwPartialWriting.objects.filter(
            (Q(first_letter_transcription__in=set_trans) | Q(second_letter_transcription__in=set_trans))
        )
        exact_pw = None
        for s in set_pw:
            if s.partial_sign == ps:
                exact_pw = s
                break
        if exact_pw:
            table_test2.append(
                (ps, exact_pw)
            )
        else:
            table_test2.append(
                (ps, None)
            )

    table_fusion = []
    prev_sign = table_test1[0][0]

    for t, f, t1, t2 in zip(table_test1, table_test2, table_true, table_false):
        print('t ', t[0])
        if t[0].get_type_number() != prev_sign.get_type_number():
            table_fusion.append(
                (None, None, None, None)
            )
            prev_sign = t[0]

        table_fusion.append(
            (t, f, t1, t2)
        )

    if request.method == "POST":
        if 'editModal' in request.POST:
            if request.POST.get("pw-id"):
                part_writing = HwPartialWriting.objects.get(pk=request.POST.get("pw-id"))
                curr_ps = HwPartialSign.objects.get(pk=request.POST.get("curr-ps"))

                ps_value = HwPartialSignValue.objects.get(pk=request.POST.get("psv"))
                if request.POST.get("psvc"):
                    ps_concretization = HwPartialSignValueConcretization.objects.get(pk=request.POST.get("psvc"))
                else:
                    ps_concretization = None

                part_sign_samp = HwPartialSign.objects.filter(partial_sign_value=ps_value,
                                                              partial_sign_value_concretization=ps_concretization,
                                                              first_letter_concretization=curr_ps.first_letter_concretization,
                                                              second_letter_concretization=curr_ps.second_letter_concretization)
                if part_sign_samp:
                    part_writing.partial_sign = part_sign_samp.first()
                else:
                    part_sign = HwPartialSign()
                    part_sign.first_letter_concretization = curr_ps.first_letter_concretization
                    part_sign.second_letter_concretization = curr_ps.second_letter_concretization
                    part_sign.partial_sign_value = ps_value
                    part_sign.partial_sign_value_concretization = ps_concretization
                    part_sign.save()
                    part_writing.partial_sign = part_sign

                part_writing.amount_sign = request.POST.get("amount-sign")

                # part_sign = HwPartialSign.objects.get(pk=part_writing.partial_sign.pk)
                # part_sign.partial_sign_value = HwPartialSignValue.objects.get(pk=request.POST.get("psv"))
                # if request.POST.get("psvc"):
                # part_sign.partial_sign_value_concretization = HwPartialSignValueConcretization.objects.get(pk=request.POST.get("psvc"))
                # else:
                # part_sign.partial_sign_value_concretization = None

                # part_sign.save()
                part_writing.save()
            else:
                part_writing = HwPartialWriting()
                part_sign = HwPartialSign()

                curr_ps = HwPartialSign.objects.get(pk=request.POST.get("curr-ps"))
                set_num = request.POST.get("curr-set")
                print(set_num)

                if set_num == 1 or set_num == '1':
                    trans = HwTranscription.objects.filter(set=test1_set).order_by('letter_place')
                    part_writing.amount_all = test1_set.amount
                elif set_num == '2':
                    trans = HwTranscription.objects.filter(set=test2_set).order_by('letter_place')
                    part_writing.amount_all = test2_set.amount
                elif set_num == '3':
                    trans = HwTranscription.objects.filter(set=true_set).order_by('letter_place')
                    part_writing.amount_all = true_set.amount
                elif set_num == '4':
                    trans = HwTranscription.objects.filter(set=false_set).order_by('letter_place')
                    part_writing.amount_all = false_set.amount

                first_tran = trans.filter(letter=curr_ps.first_letter_concretization.letter_element.letter).first()
                if curr_ps.second_letter_concretization:
                    second_tran = trans.filter(
                        letter=curr_ps.second_letter_concretization.letter_element.letter).first()
                else:
                    second_tran = None

                part_writing.first_letter_transcription = first_tran
                part_writing.second_letter_transcription = second_tran
                part_writing.amount_sign = request.POST.get("amount-sign")

                ps_value = HwPartialSignValue.objects.get(pk=request.POST.get("psv"))
                if request.POST.get("psvc"):
                    ps_concretization = HwPartialSignValueConcretization.objects.get(pk=request.POST.get("psvc"))
                else:
                    ps_concretization = None

                part_sign_samp = HwPartialSign.objects.filter(partial_sign_value=ps_value,
                                                              partial_sign_value_concretization=ps_concretization,
                                                              first_letter_concretization=curr_ps.first_letter_concretization,
                                                              second_letter_concretization=curr_ps.second_letter_concretization)
                if part_sign_samp:
                    part_writing.partial_sign = part_sign_samp.first()
                else:
                    part_sign.first_letter_concretization = curr_ps.first_letter_concretization
                    part_sign.second_letter_concretization = curr_ps.second_letter_concretization
                    part_sign.partial_sign_value = ps_value
                    part_sign.partial_sign_value_concretization = ps_concretization
                    part_sign.save()
                    part_writing.partial_sign = part_sign

                part_writing.save()
                print(part_writing.pk)

            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

    context = {
        'person': person,
        'all_pw': all_ps_types,
        'table': table_fusion
    }
    return render(request, "hw/partial_writing_v2_new.html", context)


def delete_partial_writing(request, person_id, writing_id):
    try:
        pw = HwPartialWriting.objects.get(pk=writing_id)
        pw.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwPartialWriting.DoesNotExist:
        return HttpResponseNotFound("<h2>Partial writing not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def load_part_values(request):
    partial_sign_type_id = request.GET.get('partial_sign_type')
    values = HwPartialSignValue.objects.filter(partial_sign_type_id=partial_sign_type_id)  # .order_by('name')
    return render(request, 'hw/values_dropdown_list_options.html', {'values': values})


def load_part_value_concretizations(request):
    values = HwPartialSignValueConcretization.objects.all()
    return render(request, 'hw/values_dropdown_list_options.html', {'values': values})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def load_part_fle(request):
    first_letter_id = request.GET.get('first_letter')
    values = HwLetterElement.objects.filter(letter_id=first_letter_id)
    return render(request, 'hw/values_dropdown_list_options.html', {'values': values})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def load_part_sle(request):
    second_letter_id = request.GET.get('second_letter')
    values = HwLetterElement.objects.filter(letter_id=second_letter_id)
    return render(request, 'hw/values_dropdown_list_options.html', {'values': values})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def load_part_flec(request):
    first_letter_element_id = request.GET.get('first_letter_element')
    values = HwLetterConcretization.objects.filter(letter_element_id=first_letter_element_id)
    return render(request, 'hw/values_dropdown_list_options.html', {'values': values})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def load_part_slec(request):
    second_letter_element_id = request.GET.get('second_letter_element')
    values = HwLetterConcretization.objects.filter(letter_element_id=second_letter_element_id)
    return render(request, 'hw/values_dropdown_list_options.html', {'values': values})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def load_letters(request):
    set_id = request.GET.get('set')
    # values = HwLetter.objects.filter(letter_id__in=HwTranscription.objects.filter(set_id=set_id).values('letter_id'))
    values = HwTranscription.objects.filter(set_id=set_id)
    return render(request, 'hw/values_dropdown_partial_letters.html', {'values': values})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def load_transcription(request):
    set_id = request.GET.get('set')
    transcription = HwTranscription.objects.filter(set_id=set_id)
    return render(request, 'hw/values_transcription.html', {'transcription': transcription})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def create_transcription(request, person_id):
    prev_page = request.META.get('HTTP_REFERER', '/')
    form = TransForm(person=person_id)
    if request.method == "POST":
        tran = HwTranscription()
        tran.letter = HwLetter.objects.get(letter_id=request.POST.get("letter"))
        tran.letter_place = request.POST.get("letter_place")
        tran.link_next = HwLinkNext.objects.get(link_next_id=request.POST.get("link_next"))
        tran.set = HwSet.objects.get(set_id=request.POST.get("set"))

        if request.POST.get("variant"):
            tran.variant = 1
        else:
            tran.variant = 0
        tran.save()
    else:
        return render(request, "hw/transcription.html", {'form': form,
                                                         'prev_page': prev_page})
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def delete_transcription(request, person_id, set_id):
    try:
        tran = HwTranscription.objects.filter(set_id__exact=set_id)
        for tr in tran:
            tr.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Transcription not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def part_sign(request, person_id=None):
    prev_page = '/'
    if person_id is not None:
        prev_page = reverse('partial_writing', args=[person_id])

    part_signs = HwPartialSign.objects.all().order_by('partial_sign_value__partial_sign_type__partial_sign_type_number')
    return render(request, 'hw/part_sign.html', {"part_signs": part_signs,
                                                          #"prev_page": prev_page,
                                                          'title': 'Выделенные частные признаки'})


def delete_part_sign(request, sign_id):
    try:
        sign = HwPartialSign.objects.get(pk=sign_id)
        sign.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwPartialSign.DoesNotExist:
        return HttpResponseNotFound("<h2>Partial Sign not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def part_sign_type(request, person_id=None):
    prev_page = '/'
    if person_id is not None:
        prev_page = reverse('partial_writing', args=[person_id])

    part_types = HwPartialSignType.objects.all()
    if request.method == "POST":
        if 'saveModal' in request.POST:
            part_type = HwPartialSignType.objects.get(pk=request.POST.get("type_id"))
            part_type.partial_sign_type = request.POST.get("type")
        else:
            part_type = HwPartialSignType()
            part_type.partial_sign_type = request.POST.get("partial_sign_type")
            part_type.partial_sign_type_number = HwPartialSignType.objects.aggregate(Max('partial_sign_type_number'))[
                                                     'partial_sign_type_number__max'] + 1
        part_type.save()
        return HttpResponseRedirect(request.path_info)
    else:
        return render(request, 'hw/part_sign_type.html', {"part_types": part_types,
                                                          "prev_page": prev_page,
                                                          'title': 'Частные признаки'})


def delete_part_sign_type(request, type_id):
    try:
        type = HwPartialSignType.objects.get(partial_sign_type_id=type_id)
        type.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Partial Sign Type not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def part_sign_value(request, type_id, person_id=None):
    person = None
    if person_id:
        person = person_id
    part_type = HwPartialSignType.objects.get(partial_sign_type_id=type_id)
    values = HwPartialSignValue.objects.filter(partial_sign_type_id__exact=type_id)
    if request.method == "POST":
        if 'saveModal' in request.POST:
            part_val = HwPartialSignValue.objects.get(pk=request.POST.get("value_id"))
            part_val.partial_sign_value = request.POST.get("value")
        else:
            part_val = HwPartialSignValue()
            part_val.partial_sign_type_id = type_id
            part_val.partial_sign_value = request.POST.get("partial_sign_value")
            # группа значения вычисляется в зависимости от группы признака
            part_val.partial_sign_value_number = \
                HwPartialSignValue.objects.filter(partial_sign_type_id__exact=type_id).aggregate(
                    Max('partial_sign_value_number'))['partial_sign_value_number__max'] + 1
        part_val.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        return render(request, 'hw/part_sign_value.html', {'type': part_type,
                                                           'values': values,
                                                           # 'prev_page': prev_page,
                                                           'person': person,
                                                           'title': 'Изменить значения признака'})


def delete_part_sign_value(request, type_id, value_id):
    try:
        val = HwPartialSignValue.objects.get(partial_sign_value_id=value_id)
        val.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Partial Sign Value not found</h2>")


def part_sign_value_concretization(request, type_id=None, person_id=None):
    person = None
    if person_id:
        person = person_id
    values = HwPartialSignValueConcretization.objects.all()
    if request.method == "POST":
        if 'saveModal' in request.POST:
            concr = HwPartialSignValueConcretization.objects.get(pk=request.POST.get("value_id"))
            concr.partial_sign_value_concretization = request.POST.get("value")
        else:
            concr = HwPartialSignValueConcretization()
            concr.partial_sign_value_concretization = request.POST.get("partial_sign_value_concretization")
            concr.partial_sign_value_concretization_number = \
                HwPartialSignValueConcretization.objects.all().aggregate(
                    Max('partial_sign_value_concretization_number'))[
                    'partial_sign_value_concretization_number__max'] + 1
        concr.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        return render(request, 'hw/part_sign_value_concretization.html', {'values': values,
                                                                          'type_id': type_id,
                                                                          'person': person})


def delete_part_sign_value_concretization(request, concr_id):
    try:
        val = HwPartialSignValueConcretization.objects.get(pk=concr_id)
        val.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Partial Sign Value Concretization not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def letter(request, person_id=None):
    person = None
    if person_id:
        person = person_id
    letters = HwLetter.objects.all()
    print(letters)
    if request.method == "POST":
        if 'saveModal' in request.POST:
            letter = HwLetter.objects.get(pk=request.POST.get("letter_id"))
            print(request.POST.get("letter"))
            letter.letter = request.POST.get("letter")
        else:
            letter = HwLetter()
            letter.letter = request.POST.get("new_letter")
            letter.letter_number = HwLetter.objects.all().aggregate(
                Max('letter_number'))['letter_number__max'] + 1
        letter.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        return render(request, 'hw/letter.html', {'letters': letters,
                                                  'person': person,
                                                  'title': 'Буквы'})


def delete_letter(request, letter_id):
    try:
        val = HwLetter.objects.get(pk=letter_id)
        val.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Letter not found</h2>")


def letter_element(request, letter_id, person_id=None):
    person = None
    if person_id:
        person = person_id
    letter = HwLetter.objects.get(pk=letter_id)
    values = HwLetterElement.objects.filter(letter=letter)
    all_elements = HwElement.objects.all()
    if request.method == "POST":
        if 'saveModal' in request.POST:
            letter_element = HwLetterElement.objects.get(pk=request.POST.get("value_id"))
            element = letter_element.element
            element.element = request.POST.get("value")
            element.save()
        if 'newElem' in request.POST:
            letter_element = HwLetterElement()
            element = HwElement()
            element.element = request.POST.get("element")
            element.element_number = HwElement.objects.all().aggregate(
                Max('element_number'))['element_number__max'] + 1
            letter_element.element = element
            letter_element.letter = letter
            element.save()
            letter_element.save()
        else:
            letter_element = HwLetterElement()
            letter_element.element = HwElement.objects.get(pk=request.POST.get("all_elements"))
            letter_element.letter = letter
            letter_element.save()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    else:
        return render(request, 'hw/letter_element.html',
                      {'letter': letter,
                       'values': values,
                       'all_elements': all_elements,
                       'person': person,
                       'title': 'Буквенные элементы'})


def delete_letter_element(request, letter_id, letter_element_id):
    try:
        val = HwLetterElement.objects.get(pk=letter_element_id)
        val.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Letter element not found</h2>")


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def letter_concretization(request, letter_element_id,  person_id=None, letter_id=None):
    person = None
    if person_id:
        person = person_id
    all_concretizations = HwConcretization.objects.all()
    letter_element = HwLetterElement.objects.get(letter_element_id=letter_element_id)
    concretizations = HwConcretization.objects. \
        filter(concretization_id__in=HwLetterConcretization.objects.filter(letter_element_id=letter_element_id).values(
        'concretization_id'))
    if request.method == "POST":
        if 'saveModal' in request.POST:
            concretization = HwConcretization.objects.get(pk=request.POST.get("value_id"))
            concretization.concretization = request.POST.get("value")
            concretization.save()
        else:
            if 'newConcr' in request.POST:
                concretization = HwConcretization()
                concretization.concretization = request.POST.get("concretization_value")
                concretization.concretization_number = HwConcretization.objects.all().aggregate(
                    Max('concretization_number'))['concretization_number__max'] + 1
                concretization.save()
            else:
                concretization = HwConcretization.objects.get(pk=request.POST.get("all_concr"))

            letter_concretization = HwLetterConcretization(letter_element=letter_element,
                                                           concretization=concretization)
            letter_concretization.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

    return render(request, 'hw/letter_element_concretization.html', {'letter_element': letter_element,
                                                                     'letter_id': letter_id,
                                                                     'values': concretizations,
                                                                     'person': person,
                                                                     'all_concretizations': all_concretizations})


def delete_concretization(request, concretization_id):
    try:
        concr = HwConcretization.objects.get(pk=concretization_id)
        concr.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Concretization not found</h2>")


def delete_letter_concretization(request, letter_element_id, concretization_id):
    try:
        lc = HwLetterConcretization.objects.get(letter_element_id=letter_element_id,
                                                concretization_id=concretization_id)
        lc.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except HwSet.DoesNotExist:
        return HttpResponseNotFound("<h2>Concretization not found</h2>")


def form_jsm_struct_general(initial_set):
    o = []
    types = []
    for s in initial_set:
        gw = HwGeneralWriting.objects.filter(set=s)
        gv = set()
        for g in gw:
            gv.add(g.general_sign_value.jsm_number)
            types.append(g.general_sign_value.get_type_number())
        o.append([gv, s.person])
    return o, types


def form_jsm_struct_partial(initial_set):
    o = []
    types = []
    for s in initial_set:
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        pv = set()
        for p in pw:
            pv.add(p.partial_sign.jsm_number)
            types.append(str(p.partial_sign.get_type_number()).replace(' ', ''))
        o.append([pv, s.person])
    return o, types


def form_jsm_struct_full(initial_set, is_all_signs):
    o = set()
    types = []
    val_amount = {}
    if is_all_signs:
        for s in initial_set:
            gw = HwGeneralWriting.objects.filter(set=s)
            for g in gw:
                o.add(g.general_sign_value.jsm_number)
                types.append(g.general_sign_value.get_type_number())
                val_amount[g.general_sign_value.jsm_number] = 1

    for s in initial_set:
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        for p in pw:
            o.add(p.partial_sign.jsm_number)
            types.append(p.partial_sign.get_type_number())
            val_amount[p.partial_sign.jsm_number] = p.amount_sign
    return o, types, val_amount


def form_jsm_struct_full_obj(initial_set, is_all_signs=True):
    o = set()
    if is_all_signs:
        for s in initial_set:
            gw = HwGeneralWriting.objects.filter(set=s)
            for g in gw:
                o.add(g.general_sign_value)

    for s in initial_set:
        pw = HwPartialWriting.objects.filter(first_letter_transcription__set=s)
        for p in pw:
            o.add(p.partial_sign)

    return o


def discrepant_hypothesis_types(hypothesis):
    types = set()
    # gv = HwGeneralSignValue.objects.filter(jsm_number__in=hypothesis)
    ps = HwPartialSign.objects.filter(jsm_number__in=hypothesis)

    # for g in gv:
    # types.add(g.get_type_number())
    for p in ps:
        types.add(p.get_type_number())

    return types


def decision(p_plus, p_minus):
    # Если P+>0 и | P-|>0 и (P+ - | P-|>5 или
    # P+= 0 или | P-|= 0 и (P+ - | P-|)>3, то вывод – T – подлинный.

    # Если P+>0 и | P-|>0 и (| P-|- P+ - | P-|)=5 или
    # P+= 0 или | P-|= 0 и (P+ - | P-|)=3, то вывод – T – вероятно подлинный.

    # Если P+>0 и | P-|>0 и (|P-|- P+)>5 или
    # P+= 0 или | P-|= 0 и (| P-|- P+ )>3, то вывод – T – поддельный

    # Если P+>0 и | P-|>0 и (|P-|- P+)=5 или
    # P+= 0 или | P-|= 0 и (| P-|- P+ )=3, то вывод – T – вероятно поддельный

    # Если P+>0 и | P-|>0 и (|P-|- P+) < 5 или
    # P+= 0 или | P-|= 0 и (| P-|- P+ ) < 3, то вывод сделать нельзя.

    if (p_plus > 0 and abs(p_minus) > 0 and p_plus - abs(p_minus) > 5) or \
            ((p_plus == 0 or p_minus == 0) and p_plus - abs(p_minus) > 3):
        return 'ПОДЛИННЫЙ'

    if (p_plus > 0 and abs(p_minus) > 0 and abs(p_minus) - p_plus - abs(p_minus) == 5) or \
            ((p_plus == 0 or p_minus == 0) and p_plus - abs(p_minus) == 3):
        return 'ВЕРОЯТНО ПОДЛИННЫЙ'

    if (p_plus > 0 and abs(p_minus) > 0 and abs(p_minus) - p_plus > 5) or \
            ((p_plus == 0 or p_minus == 0) and abs(p_minus) - p_plus > 3):
        return 'ПОДДЕЛЬНЫЙ'

    if (p_plus > 0 and abs(p_minus) > 0 and abs(p_minus) - p_plus == 5) or \
            ((p_plus == 0 or p_minus == 0) and abs(p_minus) - p_plus == 3):
        return 'ВЕРОЯТНО ПОДДЕЛЬНЫЙ'

    if (p_plus > 0 and abs(p_minus) > 0 and abs(p_minus) - p_plus < 5) or \
            ((p_plus == 0 or p_minus == 0) and abs(p_minus) - p_plus < 3):
        return 'ВЫВОД СДЕЛАТЬ НЕЛЬЗЯ'


def get_plus_des_hypothesis(person):
    initial_plus = HwSet.objects.filter(person=person, verity=1)
    initial_minus = HwSet.objects.filter(
        Q(person=person) & Q(verity=0) & (Q(test_number=0) | Q(test_number__isnull=True)))

    a_plus = form_jsm_struct_full_obj(initial_plus)
    a_minus = form_jsm_struct_full_obj(initial_minus)

    return a_plus - a_minus, a_plus & a_minus


def identification(person, test_signature, is_all_signs=True):
    # A+, A-
    #sign_set = 0
    initial_plus = HwSet.objects.filter(person=person, verity=1)
    initial_minus = HwSet.objects.filter(
        Q(person=person) & Q(verity=0) & (Q(test_number=0) | Q(test_number__isnull=True)))

    a_plus, b_types, a_plus_val_amount = form_jsm_struct_full(initial_plus, is_all_signs)
    a_minus, c_types, a_minus_val_amount = form_jsm_struct_full(initial_minus, is_all_signs)

    # B + гипотезы, C - гипотезы
    b = a_plus - a_minus
    c = a_minus - a_plus

    discrepant_hypothesis = a_plus & a_minus
    discrepant_h_types = discrepant_hypothesis_types(discrepant_hypothesis)

    # T
    if test_signature != 0:
        t, t_types, t_val_amount = form_jsm_struct_full([test_signature], is_all_signs)


        # X – подмножество B, входящее в T
        # Y - подмножество C, входящее в T
        x = b & t
        y = c & t

        # Z - подмножество B, не входящее в T
        # V - подмножество C, не входящее в T
        z = b - x
        v = c - y

        #  Если в B или C есть признак, входящий с разными значениями,
        # а в T входит одно из значений этого признака, то остальные значения этого признака не включаются в Z или V.

        # находим повторяющиеся типы в b
        pattern = re.compile("^[0-9]+;$")
        b_dupl_types = []
        seen = set()
        for b_t in b_types:
            if b_t in seen: #and not pattern.match(b_t):
                b_dupl_types.append(b_t)
            seen.add(b_t)

        c_dupl_types = []
        seen = set()
        for c_t in c_types:
            if c_t in seen: #and not pattern.match(c_t):
                c_dupl_types.append(c_t)
            seen.add(c_t)


        # находим повторяющиеся, которые встретились в t и удаляем их из z

        for item in list(z):
            for dupl in set(b_dupl_types) & set(t_types):
                if dupl in item:
                    print('remove ', item, ' ', dupl)
                    z.remove(item)

        for item in list(z):
            for dupl in set(c_dupl_types) & set(t_types):
                if dupl in item:
                    z.remove(item)

        # находим повторяющиеся, которые встретились в t и удаляем их из v
        for item in list(v):
            for dupl in set(c_dupl_types) & set(t_types):
                if dupl in item:
                    v.remove(item)

        for item in list(v):
            for dupl in set(b_dupl_types) & set(t_types):
                if dupl in item:
                    v.remove(item)


        # удаляем из z и v все общие признаки

        pattern = re.compile("^[0-9]+;[0-9]+$")
        for item in list(z):
            if pattern.match(item):
                z.remove(item)

        for item in list(v):
            if pattern.match(item):
                v.remove(item)

        for item in list(z):
            for type in discrepant_h_types & set(t_types):
                if type in item:
                    z.remove(item)

        for item in list(v):
            for type in discrepant_h_types & set(t_types):
                if type in item:
                    v.remove(item)


        # Присвоить веса:
        # для каждого значения каждого признака из X вес равен +1, если n1 ≤ 3, +2- если 3< n1≤ 6, +3, если 6< n1≤ 10
        # для каждого значения каждого признака из Y вес равен -1, если n2 ≤ 3, +2- если 3< n2≤ 6, +3,
        # если 6< n2≤ 10
        # для каждого значения каждого признака из Z вес равен -1,5, если n1 ≤ 3, -3- если 3< n1≤ 6, ---
        # 4,5, если 6< n1≤ 10
        # для каждого значения каждого признака из V вес равен +1,5, если n2 ≤ 3, +3- если 3< n2≤ 6, -
        # +4,5, если 6< n2 ≤ 10

        x_val_amount = {val_key: a_plus_val_amount[val_key] for val_key in x}
        y_val_amount = {val_key: a_minus_val_amount[val_key] for val_key in y}
        z_val_amount = {val_key: a_plus_val_amount[val_key] for val_key in z}
        v_val_amount = {val_key: a_minus_val_amount[val_key] for val_key in v}

        t_val_amount = {val_key: t_val_amount[val_key] for val_key in t}


        # Найти вес гипотезы
        # P+ = сумма всех плюсов, P- = сумма всех минусов.
        # Вес гипотезы P = P+ - | P-|.
        p_plus = 0
        p_minus = 0

        for val in x_val_amount:
            n1 = x_val_amount.get(val)
            if n1 is not None:
                if n1 <= 3:
                    p_plus += 1
                elif n1 > 3 and n1 <= 6:
                    p_plus += 2
                elif n1 > 6 and n1 <= 10:
                    p_plus += 3

        for val in z_val_amount:
            n1 = z_val_amount.get(val)
            if n1 is not None:
                if n1 <= 3:
                    p_minus -= 1.5
                elif n1 > 3 and n1 <= 6:
                    p_minus -= 3
                elif n1 > 6 and n1 <= 10:
                    p_minus -= 4.5

        for val in y_val_amount:
            n2 = y_val_amount.get(val)
            if n2 is not None:
                if n2 <= 3:
                    p_minus -= 1
                elif n2 > 3 and n2 <= 6:
                    p_minus -= 2
                elif n2 > 6 and n2 <= 10:
                    p_minus -= 3

        for val in v_val_amount:
            n2 = v_val_amount.get(val)
            if n2 is not None:
                if n2 <= 3:
                    p_plus += 1.5
                elif n2 > 3 and n2 <= 6:
                    p_plus += 3
                elif n2 > 6 and n2 <= 10:
                    p_plus += 4.5



        '''
        print('P+ ', p_plus)
        print('P- ', p_minus)
        '''

        return x_val_amount,\
               y_val_amount,\
               z_val_amount, \
               v_val_amount,\
               t_val_amount,\
               p_plus - abs(p_minus),\
               decision(p_plus, p_minus)

    return b, discrepant_hypothesis

def get_objects(val_amount):
    if type(val_amount) == dict:
        obj_val_amount = {}

        for val_key in val_amount:
            gen_obj = HwGeneralSignValue.objects.filter(jsm_number=val_key)
            part_obj = HwPartialSign.objects.filter(jsm_number=val_key)
            if gen_obj:
                obj_val_amount[gen_obj.first()] = val_amount[val_key]
            else:
                obj_val_amount[part_obj.first()] = val_amount[val_key]
        return obj_val_amount

    all_obj = []
    gen_obj = HwGeneralSignValue.objects.filter(jsm_number__in=val_amount)
    part_obj = HwPartialSign.objects.filter(jsm_number__in=val_amount)
    for o in gen_obj:
        all_obj.append(o)

    for o in part_obj:
        all_obj.append(o)

    return all_obj


def signature_identification(request):
    persons = Person.objects.all()
    try:
        person = Person.objects.get(pk=request.GET.get("person"))
        persons = Person.objects.exclude(pk=person.pk)
        #print(request.GET.get("test_signature"))
        if request.GET.get("test_signature") !='':
            test_signature = HwSet.objects.get(pk=request.GET.get("test_signature"))
        else:
            test_signature = HwSet.objects.get(pk=request.GET.get("test_signature2"))
        other_tests = HwSet.objects.filter(Q(person=person) & ~Q(test_number='0') & ~Q(pk=test_signature.pk))#.exclude(pk=test_signature.pk, test_number='0')
        print(other_tests)
        is_all_signs = True if request.GET.get("signs") == 'all_signs' else False
        # print('SIGNS', signs)
        x, y, z, v, t, p, decision = identification(person, test_signature, is_all_signs)
        print(get_objects(v))

        context = {
            'persons': persons,
            'selected_person': person,
            'selected_test': test_signature,
            'other_tests': other_tests,
            'is_all_signs': is_all_signs,
            'x': get_objects(x),
            'y': get_objects(y),
            'z': get_objects(z),
            'v': get_objects(v),
            't': get_objects(t),
            'p': p,
            'decision': decision,
            'title': 'Идентификация подписи'
        }
    except Person.DoesNotExist or HwSet.DoesNotExist:
        context = {
            'persons': persons,
            'title': 'Идентификация подписи'
        }

    return render(request, 'hw/signature_identification.html', context)


def group_signs(request):
    all_des_types = []
    all_plus_types = []
    

    for person in Person.objects.all():
        plus_h, des_h = get_plus_des_hypothesis(person)
        for h in plus_h:
         all_plus_types.append(h.get_sign_type_object())

        for h in des_h:
         all_des_types.append(h.get_sign_type_object())


    plus_dict = {}
    des_dict = {}

    for type in all_plus_types:
        if type in plus_dict:
            plus_dict[type] = plus_dict[type] + 1
        else:
            plus_dict[type] = 1

    for type in all_des_types:
        if type in des_dict:
            des_dict[type] = des_dict[type] + 1
        else:
            des_dict[type] = 1

    all_types = HwPartialSignType.objects.all() #set(plus_dict.keys()) | set(des_dict.keys())
    table = set()
    for key in all_types:
        table.add((key, plus_dict[key] if key in plus_dict else 0, des_dict[key] if key in des_dict else 0))

    amount_plus = 0
    for key in plus_dict:
        amount_plus += plus_dict[key]

    amount_des = 0
    for key in des_dict:
        amount_des += des_dict[key]

    return render(request, 'hw/group_signs.html', {'table': table, 'amount_plus': amount_plus, 'amount_des': amount_des})


# исследование информативности признаков
def informativeness_research(request):
    pws = HwPartialWriting.objects.filter(partial_sign__jsm_number='2;64:6,6;1,0')
    sign = HwPartialSign.objects.get(jsm_number='2;64:8,1;1,0')

    for pw in pws:
        pw.partial_sign = sign
        pw.save()

    all_des = {}
    for person in Person.objects.all():
        plus_h, des_h = get_plus_des_hypothesis(person)
        for h in des_h:
            if h in all_des:
                all_des[h] = all_des[h]+1
            else:
                all_des[h] = 1

    uninformative = {val_key: all_des[val_key] for val_key in all_des if all_des[val_key] > 2}

    return render(request, 'hw/uninformative_signs.html', {'uninformative': uninformative,})