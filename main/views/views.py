from django.shortcuts import render
from main.models import *
from django.db.models import Q

from main.views.decorators import allowed_users
from django.contrib.auth.decorators import login_required

from main.domain.jsm_func import *


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def home_page(request):
    return render(request, 'general/home.html', context={'title': "Главная"})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def jsm(request):
    # операция сходства
    # POSITIVE

    # берем положительные множества и формируем структуру [{'1;2', '2;3' ,... }, person]
    # oplus = [ [{'1;2', '2;3' ,... }, person1], [{'1;2', '2;4' ,... }, person2], [{'1;1', '2;5' ,... }, preson3],...]

    oplus = []
    for s in HwSet.objects.filter(verity=1).order_by('person__person_id'):
        gw = HwGeneralWriting.objects.filter(set=s)
        gv = set()
        for g in gw:
            gv.add(g.general_sign_value.get_number())
        oplus.append([gv, s.person])

    ominus = []
    for s in HwSet.objects.filter(Q(verity=0) & (Q(test_number=0) | Q(test_number__isnull=True))).order_by(
            'person__person_id'):
        gw = HwGeneralWriting.objects.filter(set=s)
        gv = set()
        for g in gw:
            gv.add(g.general_sign_value.get_number())
        ominus.append([gv, s.person])

    # формируем список [[{мн-во неинф призн для человека}, чел], [], ...     ]
    uninform = []
    for desc in oplus:
        neg_desc = [x for x in ominus if x[1] == desc[1]]
        if neg_desc:
            uninform.append([desc[0] & neg_desc[0][0], desc[1]])

    # находим сходства
    cpcplus = norris(oplus)
    cpcminus = norris(ominus)

    # Фильтруем [ [{'',''},{pp}], [] ]
    filtered_cpcplus = jsm_filtration(cpcplus)
    filtered_cpcminus = jsm_filtration(cpcminus)

    # формируем список с объектами вместо номеров
    ocplus = []
    for f in filtered_cpcplus:
        obj = HwGeneralSignValue.objects.filter(jsm_number__in=f[0]).order_by(
            'general_sign_type__general_sign_type_number')
        ocplus.append([obj, f[1]])

    ocminus = []
    for f in filtered_cpcminus:
        obj = HwGeneralSignValue.objects.filter(jsm_number__in=f[0]).order_by(
            'general_sign_type__general_sign_type_number')
        ocminus.append([obj, f[1]])

    sorted_ocplus, biggest_hypothesis = sort_hypotheses_2(ocplus)

    # for h in biggest_hypothesis:
    #     print(h)
    print(len(sorted_ocplus))

    return render(request, 'hw/jsm.html', {'sim_plus': sorted_ocplus,
                                             'sim_minus': sort_hypotheses(ocminus),
                                             'title': 'Сходства по общим признакам'})


# глобальные переменные
filtered_cpcplus_trans = []
filtered_cpcminus_trans = []
index = 0


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def jsm_ps_minus(request, indx):
    global filtered_cpcminus_trans, index
    index = indx

    general_set = filtered_cpcminus_trans[index]
    print('TEST: ', general_set)
    ps_ominus = []
    for person in general_set[2]:
        p_set = HwSet.objects.filter(Q(person=person) & Q(verity=0) & (Q(test_number=0) | Q(test_number__isnull=True)))
        if p_set:
            p_pw = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                   first_letter_transcription__letter__letter_number__in=general_set[0])
            pw = set()
            for p in p_pw:
                pw.add(p.partial_sign.jsm_number)

            p_pw_gen_first = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                             partial_sign__first_letter_concretization__letter_element__general_element__general_element_number__in=
                                                             general_set[1])
            p_pw_opt_gen_first = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                                 partial_sign__first_letter_concretization__letter_element__optional_general_element__general_element_number__in=
                                                                 general_set[1])

            p_pw_gen_sec = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                           partial_sign__second_letter_concretization__letter_element__general_element__general_element_number__in=
                                                           general_set[1])
            p_pw_opt_gen_sec = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                               partial_sign__second_letter_concretization__letter_element__optional_general_element__general_element_number__in=
                                                               general_set[1])
            all_p_pw_gen = p_pw_gen_first.union(p_pw_opt_gen_first, p_pw_gen_sec, p_pw_opt_gen_sec)
            # формируем номера
            pw_gen = set()
            for p in all_p_pw_gen:
                pw_gen.add(p.partial_sign.general_jsm_number)
                pw_gen.add(p.partial_sign.opt_general_jsm_number)

        ps_ominus.append([pw, pw_gen, person])

    ps_cpсminus = norris(ps_ominus, True)
    filtered_ps_cpcminus = jsm_filtration_trans(ps_cpсminus)

    ps_ocminus = []
    for f in filtered_ps_cpcminus:
        ps_obj = HwPartialSign.objects.filter(jsm_number__in=f[0])
        ps_gen_obj = HwPartialSign.objects.filter(general_jsm_number__in=f[1])
        ps_opt_gen_obj = HwPartialSign.objects.filter(opt_general_jsm_number__in=f[1])
        ps_ocminus.append([ps_obj, ps_gen_obj | ps_opt_gen_obj, f[2]])

    return render(request, 'hw/jsm_ps.html', {'general_set': general_set, 'ps_sim': sort_hypotheses(ps_ocminus, 2)})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def jsm_ps(request, indx):
    global filtered_cpcplus_trans, index
    index = indx

    general_set = filtered_cpcplus_trans[index]
    print('TEST: ', general_set)
    ps_oplus = []
    for person in general_set[2]:
        p_set = HwSet.objects.filter(person=person, verity=1)
        if p_set:
            p_pw = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                   first_letter_transcription__letter__letter_number__in=general_set[0])
            pw = set()
            for p in p_pw:
                pw.add(p.partial_sign.jsm_number)

            p_pw_gen_first = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                             partial_sign__first_letter_concretization__letter_element__general_element__general_element_number__in=
                                                             general_set[1])
            p_pw_opt_gen_first = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                                 partial_sign__first_letter_concretization__letter_element__optional_general_element__general_element_number__in=
                                                                 general_set[1])

            p_pw_gen_sec = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                           partial_sign__second_letter_concretization__letter_element__general_element__general_element_number__in=
                                                           general_set[1])
            p_pw_opt_gen_sec = HwPartialWriting.objects.filter(first_letter_transcription__set__in=p_set,
                                                               partial_sign__second_letter_concretization__letter_element__optional_general_element__general_element_number__in=
                                                               general_set[1])
            all_p_pw_gen = p_pw_gen_first.union(p_pw_opt_gen_first, p_pw_gen_sec, p_pw_opt_gen_sec)
            # формируем номера
            pw_gen = set()
            for p in all_p_pw_gen:
                pw_gen.add(p.partial_sign.general_jsm_number)
                pw_gen.add(p.partial_sign.opt_general_jsm_number)

        ps_oplus.append([pw, pw_gen, person])

    ps_cpсplus = norris(ps_oplus, True)
    filtered_ps_cpcplus = jsm_filtration_trans(ps_cpсplus)

    ps_ocplus = []
    for f in filtered_ps_cpcplus:
        ps_obj = HwPartialSign.objects.filter(jsm_number__in=f[0])
        ps_gen_obj = HwPartialSign.objects.filter(general_jsm_number__in=f[1])
        ps_opt_gen_obj = HwPartialSign.objects.filter(opt_general_jsm_number__in=f[1])
        ps_ocplus.append([ps_obj, ps_gen_obj | ps_opt_gen_obj, f[2]])

    print(ps_ocplus)
    return render(request, 'hw/jsm_ps.html', {'general_set': general_set, 'ps_sim': sort_hypotheses(ps_ocplus, 2)})


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def jsm_trans(request):
    # операция сходства для транскрипций
    global filtered_cpcplus_trans, filtered_cpcminus_trans
    # формируем структуру [{123, 34,..(номера букв)}, {1,2,3 (general elements)}, person]
    oplus = []
    for s in HwSet.objects.filter(verity=1).order_by('person__person_id'):
        tr = HwTranscription.objects.filter(set=s)
        letters = set()
        gen_elements = set()
        for t in tr:
            if t.letter.__str__() != 'подпись':
                letters.add(t.letter.letter_number)
            elements = HwLetterElement.objects.filter(letter=t.letter).values('general_element__general_element_number')
            optional_elements = HwLetterElement.objects.filter(letter=t.letter).values(
                'optional_general_element__general_element_number')
            for e in elements:
                if e['general_element__general_element_number']:
                    gen_elements.add(e['general_element__general_element_number'])
            for e in optional_elements:
                if e['optional_general_element__general_element_number']:
                    gen_elements.add(e['optional_general_element__general_element_number'])

        oplus.append([letters, gen_elements, s.person])

    ominus = []
    for s in HwSet.objects.filter(Q(verity=0) & (Q(test_number=0) | Q(test_number__isnull=True))).order_by(
            'person__person_id'):
        tr = HwTranscription.objects.filter(set=s)
        letters = set()
        gen_elements = set()
        for t in tr:
            if t.letter.__str__() != 'подпись':
                letters.add(t.letter.letter_number)
            elements = HwLetterElement.objects.filter(letter=t.letter).values('general_element__general_element_number')
            optional_elements = HwLetterElement.objects.filter(letter=t.letter).values(
                'optional_general_element__general_element_number')
            for e in elements:
                if e['general_element__general_element_number']:
                    gen_elements.add(e['general_element__general_element_number'])
            for e in optional_elements:
                if e['optional_general_element__general_element_number']:
                    gen_elements.add(e['optional_general_element__general_element_number'])

        ominus.append([letters, gen_elements, s.person])

    cpсplus = norris(oplus, True)
    cpсminus = norris(ominus, True)
    filtered_cpcplus_trans = jsm_filtration_trans(cpсplus)
    filtered_cpcminus = jsm_filtration_trans(cpсminus)

    # формируем список с объектами вместо номеров
    ocplus = []
    for f in filtered_cpcplus_trans:
        letter_obj = HwLetter.objects.filter(letter_number__in=f[0]).order_by('letter_number')
        element_obj = HwGeneralElement.objects.filter(general_element_number__in=f[1]).order_by(
            'general_element_number')
        ocplus.append([letter_obj, element_obj, f[2]])

    ocminus = []
    for f in filtered_cpcminus:
        letter_obj = HwLetter.objects.filter(letter_number__in=f[0]).order_by('letter_number')
        element_obj = HwGeneralElement.objects.filter(general_element_number__in=f[1]).order_by(
            'general_element_number')
        ocminus.append([letter_obj, element_obj, f[2]])

    return render(request, 'hw/jsm_2.html', {'sim_plus': ocplus,
                                               'range_plus': range(len(ocplus)),
                                               'sim_minus': ocminus,
                                               'range_minus': range(len(ocminus))})
