from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.db.models import Max, Sum, Count
from django.db.models.functions import Coalesce
from django.shortcuts import render, redirect
from django.db.models import Q

from .decorators import unauthenticated_user, allowed_users, admin_only
from django.contrib.auth.decorators import login_required

from main.models import *


from main.domain.service_functions import count_assessment, count_persons_w_tests_n_hw
from main.domain.model_getters import get_assessments_by_questionnaire, \
    get_results_by_person, get_results_by_scale


# отображение страницы с результатами респондента
@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def tests(request):
    person_id = request.GET.get('select_person', 1)
    questionnaire_id = request.GET.get('select_questionnaire', 1)
    partitioning_scale_name_id = request.GET.get('select_partitioning', 2)

    persons = Person.objects.all().order_by('person_surname')
    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()

    # выбрать только те шкалы, которые соответствуют опроснику (questionnaire_id)
    questions = QQuestion.objects.filter(questionnaire_id=int(questionnaire_id)).values_list('question_id', flat=True)
    scales = QQuestionScale.objects.filter(question_id__in=questions).values_list('scale_id', flat=True)

    # результаты данного респондента по данным шкалам
    results = get_results_by_person(person_id, scales).order_by('scale__number_in_result')

    assessments = get_assessments_by_questionnaire(questionnaire_id, partitioning_scale_name_id, scales)
    print(len(assessments))

    table = []
    for result in results:
        assessment = count_assessment(assessments, questionnaire_id, result)
        table.append((result.scale, result.raw_score, result.result_score,
                      assessment.partitioning_scale.interval_set.interval_name.interval_name))

    context = {
        'table': table,
        'persons': persons,
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'person_id': int(person_id),
        'questionnaire_id': int(questionnaire_id),
        'partitioning_scale_name_id': int(partitioning_scale_name_id),
        'title': "Опросники"
    }
    return render(request, 'qstnrs/tests.html', context=context)


# заполнение опросника
@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def person_test(request, person_id, questionnaire_id):

    person = Person.objects.get(person_id=person_id)
    questionnaire = QQuestionnaire.objects.get(questionnaire_id=questionnaire_id)

    selections = QPersonSelection.objects.filter(person_id=person_id,
                                                 answer__question__questionnaire_id=questionnaire_id)
    questions = QQuestion.objects.filter(questionnaire_id=questionnaire_id).order_by('number_in_questionnaire')
    scales = QQuestionScale.objects.filter(question_id__in=questions).values_list('scale_id', flat=True).distinct()
    variants = QAnswer.objects.filter(question__questionnaire_id=questionnaire_id).values_list(
        'answer_text__answer_text', 'answer_id') \
        .order_by('question', 'number_in_question')

    table = []
    for q in questions:
        num = q.number_in_questionnaire
        question = q.question_text.question_text
        question_id = q.question_id
        _variants = variants.filter(question=question_id)
        table.append((num, question, _variants, question_id))

    if request.method == "POST":
        for item in request.POST.items():
            if item[0] != 'csrfmiddlewaretoken':
                answer_id = item[1]
                person_id = person_id
                question_id = QAnswer.objects.get(answer_id=answer_id).question.question_id
                answers = QAnswer.objects.filter(question_id=question_id)
                try:
                    old_selection = QPersonSelection.objects.get(person_id=person_id, answer_id__in=answers)
                    old_selection.answer_id = answer_id
                    old_selection.save()
                except QPersonSelection.DoesNotExist:
                    QPersonSelection.objects.get_or_create(
                        person_id=person_id,
                        answer_id=answer_id
                    )

        # считаем баллы
        for scale in scales:
            questions = QQuestionScale.objects.filter(scale=scale).values_list('question_id', flat=True)
            # считаем сырой балл путем суммиования баллов за ответы
            total = selections.filter(answer__question__in=questions) \
                .aggregate(total=Coalesce(Sum('answer_id__score'), 0))['total']
            print('total: ', total)

            # для ОЧХ считаем баллы в "стенах", а значит уточняем,
            # что interval_set_name=5 (стены)
            if int(questionnaire_id) == 2:
                total = int(QPartitioning.objects.get(interval_score__min_score__lte=total,
                                                      interval_score__max_score__gte=total,
                                                      partitioning_scale__interval_set__interval_set_name=5,
                                                      partitioning_scale__scale=scale)
                            .partitioning_scale.interval_set.interval_name.interval_nickname)

            # если ответы поменялись, то удаляем из базы старые и заменяем на новые
            try:
                old_result = QResult.objects.get(person_id=person_id,
                                                scale_id=scale)
                old_result.raw_score = total
                old_result.result_score = total
                old_result.save()
            except QResult.DoesNotExist:
                QResult.objects.get_or_create(
                    person_id=person_id,
                    scale_id=scale,
                    raw_score=total,
                    result_score=total
                )

        return HttpResponseRedirect('/tests/?select_person=' + str(person_id) + '&select_questionnaire=' + str(
            questionnaire_id) + '&select_partitioning=2')

    context = {
        'person': person,
        'questionnaire': questionnaire,
        'selections': selections,
        'table': table,
        'title': person.__str__() + ': ' + questionnaire.questionnaire_nickname,
        'person_id': int(person_id),
        'questionnaire_id': int(questionnaire_id)
    }
    return render(request, 'qstnrs/person_test.html', context=context)


# отображение сводной страницы по тестам
def tests_overview(request):
    # questionnaire_id = request.GET.get('select_questionnaire', 1)
    scale_id = int(request.GET.get('select_scale', 1))
    partitioning_scale_name_id = int(request.GET.get('select_partitioning', 2))

    partitionings = QPartitioningScaleName.objects.all()

    # questionnaires = QQuestionnaire.objects.all()
    scales = QScale.objects.all()
    results = get_results_by_scale(scale_id).order_by('person__person_surname')

    questions = QQuestionScale.objects.filter(scale_id=scale_id).values_list('question_id', flat=True)

    questionnaire_id = QQuestion.objects.filter(question_id__in=questions).values_list('questionnaire_id', flat=True)[0]

    assessments = get_assessments_by_questionnaire(questionnaire_id,
                                                   partitioning_scale_name_id,scales)

    output_table = []
    for result in results:
        assessment = count_assessment(assessments, questionnaire_id, result)
        output_table.append((result.person, result.raw_score, result.result_score,
                             assessment.partitioning_scale.interval_set.interval_name.interval_name))

    def my_sort(e):
        return e[1]
    output_table.sort(key=my_sort)

    context = {
        'title': 'Сводка по шкалам',
        'output_table': output_table,
        'scales': scales,
        'partitionings': partitionings,
        'questionnaire_id': questionnaire_id,
        'scale_id': scale_id,
        'partitioning_scale_name_id': partitioning_scale_name_id
    }
    return render(request, 'qstnrs/overview.html', context=context)


def persons_with_tests_and_signature(request):
    person_list_1 = count_persons_w_tests_n_hw(1)
    person_list_2 = count_persons_w_tests_n_hw(2)
    person_list_3 = count_persons_w_tests_n_hw(3)
    person_list_4 = count_persons_w_tests_n_hw(4)
    context = {
        "person_list_1": person_list_1,
        "person_list_2": person_list_2,
        "person_list_3": person_list_3,
        "person_list_4": person_list_4
    }
    return render(request, 'qstnrs/persons_subbase.html', context=context)
