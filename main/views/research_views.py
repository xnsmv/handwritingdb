from django.shortcuts import render
from main.models import QQuestionnaire, QPartitioningScaleName, \
    HwGeneralWriting

from main.views.decorators import allowed_users
from django.contrib.auth.decorators import login_required

from main.domain.service_functions import divide_by_sex, \
    count_persons_w_tests_n_hw
from main.domain.views_functions import similarities_q_func, \
    similarities_hw_func, get_div_params, create_potential_hypotheses, classify_potential_hypotheses

import time


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def similarities_q_view(request):
    questionnaire_id = request.GET.get('select_questionnaire', 1)
    partitioning_scale_name_id = request.GET.get('select_partitioning', 2)
    resps_minimum = int(request.GET.get('resps_minimum', 2))
    q_or_hw = 'q'
    split_func = divide_by_sex

    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()

    sorted_similarities_plus, sorted_similarities_minus = similarities_q_func(questionnaire_id,
                                                                              partitioning_scale_name_id,
                                                                              resps_minimum,
                                                                              split_func, q_or_hw=q_or_hw)

    context = {
        'sim_plus': sorted_similarities_plus,
        'sim_minus': sorted_similarities_minus,
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'questionnaire_id': int(questionnaire_id),
        'partitioning_scale_name_id': int(partitioning_scale_name_id),
        'resps_minimum': int(resps_minimum),
        'quantity_plus': len(sorted_similarities_plus),
        'quantity_minus': len(sorted_similarities_minus),
        'title': 'Сходства по опросникам'
    }
    return render(request, 'research/similarities_q.html', context=context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def similarities_gw_view(request):
    resps_minimum = 2
    q_or_hw = 'hw'
    split_func = divide_by_sex
    sim_signs = 'gen_elem'

    resps_list = HwGeneralWriting.objects.prefetch_related('set').filter(
    ).values_list('set__person', flat=True).distinct()

    sorted_similarities_plus, sorted_similarities_minus = similarities_hw_func(resps_list,
                                                                               resps_minimum,
                                                                               sim_signs,
                                                                               split_func,
                                                                               q_or_hw)

    context = {
        'sim_plus': sorted_similarities_plus,
        'sim_minus': sorted_similarities_minus,
        'title': 'Сходства по общим и обобщенным частным признакам',
        'quantity_plus': len(sorted_similarities_plus),
        'quantity_minus': len(sorted_similarities_minus),
    }

    return render(request, 'research/similarities_gw.html', context=context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def similarities_general_view(request):
    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()

    questionnaire_id = int(request.GET.get('select_questionnaire', 1))
    partitioning_scale_name_id = int(request.GET.get('select_partitioning', 2))
    resps_minimum = int(request.GET.get('resps_minimum', 2))
    div_option = int(request.GET.get('div_param', 1))
    sim_signs = request.GET.get('sim_signs', 'groups')

    scale_id = 11
    div_value = 3

    split_func, div_plus, div_minus, div_params = get_div_params(div_option, scale_id, div_value)

    resps_list = count_persons_w_tests_n_hw(questionnaire_id)

    # QUESTIONNAIRES

    sorted_similarities_plus_q, sorted_similarities_minus_q = similarities_q_func(
        questionnaire_id,
        partitioning_scale_name_id,
        resps_minimum,
        split_func,
        resps_list,
        q_or_hw='q', **div_params)

    # HANDWRITINGS

    sorted_similarities_plus_hw, sorted_similarities_minus_hw = similarities_hw_func(
        resps_list,
        resps_minimum,
        sim_signs,
        split_func,
        q_or_hw='hw', **div_params)

    context = {
        'title': 'Сходства по опросникам и подписям',
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'questionnaire_id': questionnaire_id,
        'partitioning_scale_name_id': partitioning_scale_name_id,
        'resps_minimum': resps_minimum,
        'div_param': div_option,
        'sim_plus_hw': sorted_similarities_plus_hw,
        'sim_minus_hw': sorted_similarities_minus_hw,
        'sim_plus_q': sorted_similarities_plus_q,
        'sim_minus_q': sorted_similarities_minus_q,
        'quantity_plus_q': len(sorted_similarities_plus_q),
        'quantity_minus_q': len(sorted_similarities_minus_q),
        'quantity_plus_hw': len(sorted_similarities_plus_hw),
        'quantity_minus_hw': len(sorted_similarities_minus_hw),
        'is_gen_elem': True if sim_signs == 'gen_elem' else False,
        'div_plus': div_plus,
        'div_minus': div_minus
    }

    return render(request, 'research/similarities_general.html', context=context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def potential_hypotheses_view(request):
    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()
    questionnaire_id = int(request.GET.get('select_questionnaire', 1))
    partitioning_scale_name_id = int(request.GET.get('select_partitioning', 2))
    resps_minimum = int(request.GET.get('resps_minimum', 2))

    div_option = int(request.GET.get('div_param', 1))
    sim_signs = request.GET.get('sim_signs', 'groups')

    scale_id = 11
    div_value = 3

    split_func, div_plus, div_minus, div_params = get_div_params(div_option,
                                                                 scale_id,
                                                                 div_value)

    potential_hypotheses_plus, potential_hypotheses_minus, *rest = create_potential_hypotheses(
        questionnaire_id=questionnaire_id,
        partitioning_scale_name_id=partitioning_scale_name_id,
        sim_signs=sim_signs,
        resps_minimum=resps_minimum,
        split_func=split_func,
        **div_params,
    )

    return render(request, 'research/potential_hypotheses.html',
                  {'potential_hypotheses_plus': potential_hypotheses_plus,
                   'potential_hypotheses_minus': potential_hypotheses_minus,
                   'questionnaires': questionnaires,
                   'partitionings': partitionings,
                   'questionnaire_id': questionnaire_id,
                   'partitioning_scale_name_id':
                       partitioning_scale_name_id,
                   'resps_minimum': resps_minimum,
                   'title': 'Потенциальные гипотезы',
                   'quantity_plus': len(potential_hypotheses_plus),
                   'quantity_minus': len(potential_hypotheses_minus),
                   'div_param': div_option,
                   'is_gen_elem': True if sim_signs == 'gen_elem' else False,
                   'div_plus': div_plus,
                   'div_minus': div_minus
                   })


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin', 'expert'])
def jsm_choose_option_view(request):
    # t0=time.time()
    questionnaires = QQuestionnaire.objects.all()
    partitionings = QPartitioningScaleName.objects.all()
    questionnaire_id = int(request.GET.get('select_questionnaire', 1))
    partitioning_scale_name_id = int(request.GET.get('select_partitioning', 2))
    resps_minimum = int(request.GET.get('resps_minimum', 2))

    div_option = int(request.GET.get('div_param', 1))
    sim_signs = request.GET.get('sim_signs', 'groups')

    scale_id = 11
    div_value = 3

    split_func, div_plus, div_minus, div_params = get_div_params(div_option,
                                                                 scale_id,
                                                                 div_value)
    # t0 = time.time()
    sorted_sim_all_plus, sorted_sim_all_minus, sorted_hypotheses_hw_plus, sorted_hypotheses_hw_minus, sorted_hypotheses_q_plus, sorted_hypotheses_q_minus = create_potential_hypotheses(
        questionnaire_id=questionnaire_id,
        partitioning_scale_name_id=partitioning_scale_name_id,
        sim_signs=sim_signs,
        resps_minimum=resps_minimum,
        split_func=split_func,
        **div_params
    )
    # print('potential hypo:', time.time()-t0)
    # t1 = time.time()
    direct_jsm_plus, inverse_jsm_plus, both_jsm_plus, neither_jsm_plus, direct_jsm_minus, \
    inverse_jsm_minus, both_jsm_minus, neither_jsm_minus = classify_potential_hypotheses(sorted_sim_all_plus,
                                                                                         sorted_sim_all_minus,
                                                                                         sorted_hypotheses_hw_plus,
                                                                                         sorted_hypotheses_hw_minus,
                                                                                         sorted_hypotheses_q_plus,
                                                                                         sorted_hypotheses_q_minus)
    # print('classify:', time.time()-t1)
    quantity_plus = len(sorted_sim_all_plus)
    quantity_minus = len(sorted_sim_all_minus)
    context = {
        'questionnaires': questionnaires,
        'partitionings': partitionings,
        'questionnaire_id': questionnaire_id,
        'partitioning_scale_name_id': partitioning_scale_name_id,
        'resps_minimum': resps_minimum,
        'div_param': div_option,
        'quantity_plus': quantity_plus,
        'quantity_minus': quantity_minus,
        'direct_hypotheses_plus': direct_jsm_plus,
        'direct_quantity_plus': len(direct_jsm_plus),
        'inverse_hypotheses_plus': inverse_jsm_plus,
        'inverse_quantity_plus': len(inverse_jsm_plus),
        'both_hypotheses_plus': both_jsm_plus,
        'both_quantity_plus': len(both_jsm_plus),
        'neither_hypotheses_plus': neither_jsm_plus,
        'neither_quantity_plus': len(neither_jsm_plus),
        'direct_hypotheses_minus': direct_jsm_minus,
        'direct_quantity_minus': len(direct_jsm_minus),
        'inverse_hypotheses_minus': inverse_jsm_minus,
        'inverse_quantity_minus': len(inverse_jsm_minus),
        'both_hypotheses_minus': both_jsm_minus,
        'both_quantity_minus': len(both_jsm_minus),
        'neither_hypotheses_minus': neither_jsm_minus,
        'neither_quantity_minus': len(neither_jsm_minus),
        'is_gen_elem': True if sim_signs == 'gen_elem' else False,
        'title': 'Выбор метода',
        'div_plus': div_plus,
        'div_minus': div_minus
    }
    # print('total:', time.time()-t0)
    return render(request, 'research/jsm_choose_option.html', context=context)
